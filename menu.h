#ifndef MENU_H_INCLUDED
#define MENU_H_INCLUDED
#include "Struct.h"

#define NOMBRE_CARTES 6  //Nombre de cartes g�n�r�es

void Lancer_Menu(SDL_Surface* screen, Choix *set, int *map);
void Init_Menu(SDL_Surface* screen, SDL_Surface **boutons, SDL_Surface **sousmenu, Choix *set, int *map);
void Fondu_apparition(SDL_Rect *but, SDL_Surface* screen, SDL_Surface* bouton, SDL_Rect *destination);
void Fondu_disparition(SDL_Surface *fond, SDL_Surface *screen);
void Event_Menu(SDL_Surface *screen, SDL_Rect *but, SDL_Rect *destination1, SDL_Rect *destination2, SDL_Rect *destination3, SDL_Surface* *boutons, SDL_Surface* *sousmenu, Choix *set, int *map);
int Menu_single(SDL_Surface *screen, int y, Choix *set, SDL_Surface **sousmenu);//retourne le choix du joueur -1 et -2 si il quite si il revient au menu//les coordon�es a partir desquelles on peut afficher des boutons
int Menu_multiple(SDL_Surface *screen, int y, Choix *set, SDL_Surface **sousmenu);
int Choix_map(SDL_Surface *screen, int *map);
void Scores(jeu *game, SDL_Surface *screen, int *winner, int *comp, int nwin);



#endif // JEU_H_INCLUDED
