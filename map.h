#ifndef MAP_H_INCLUDED
#define MAP_H_INCLUDED
#include "Struct.h"

void ajouter_danger_carte(Bombe labombe, int** carte, int carte_danger[][NB_BLOC_Y]);
void enlever_danger_carte(Bombe labombe, int** carte, int carte_danger[][NB_BLOC_Y]);
int** randmap(int **tab, jeu* lejeu);
int** ChargementMap1(int ** map);
int** ChargementMap2(int ** map);
int** ChargementMap3(int ** map);
int** ChargementMap4(int ** map);
int** ChargementMap5(int ** map);
int randomcailloux();

#endif // MAP_H_INCLUDED
