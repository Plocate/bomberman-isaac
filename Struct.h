#ifndef STRUCT_H_INCLUDED
#define STRUCT_H_INCLUDED

#define NB_BLOC_X 18 //Nombre de blocs en largeur
#define NB_BLOC_Y 12 //Nombre de blocs en hauteur
#define TAILLE_BLOC 34 //Taille d'un bloc
#define LARGEUR_FENETRE TAILLE_BLOC*NB_BLOC_X //=1020
#define HAUTEUR_FENETRE TAILLE_BLOC*NB_BLOC_Y //=680
#define PROPORTION_ROCHERS 50 //Proportion probable de rochers destructibles
#define PROPORTION_BLOCS 30 //Proportion probable de blocs indestructibles
//Les variables et constantes "vitesse" portent tr�s mal leur noms.
#define VITESSE_INITIALE 300 //temps (en ms) que prend un joueur pour parcourir une case au d�but.
#define VITESSE_BONUS 230 //temps (en ms) que prend un joueur pour parcourir une case avec le bonus.

#define TRUE 1
#define FALSE 0
typedef int bool;


enum {BAS, HAUT, DROITE, GAUCHE};
enum {VIDE, ROCHER, BLOC, MUR, BOMBE, MBOMB, MHEART, TIME, SPEED, RANGE};


typedef struct t_bombe{

    short num_joueur; //Num�ro du joueur qui l'a pos�e.
    short range; //port�e d'explosion de la bombe
    int temps; //temps restant avant explosion de la bombe
	SDL_Rect pos;
	int posx;
	int posy;
	bool posee;
}Bombe;

typedef struct {
	char key[SDLK_LAST];
}Input;

typedef struct t_joueur{

    short num; //num�ro qui identifie le joueur
    bool est_IA;
    short vie; //nombre de pv restants
    int pos_x; //Position en x et y sur la grille de jeu.
    int pos_y;
    bool se_deplace; //Le joueur est-il en mouvement ?
    int vitesse; //Temps en millisecondes pour franchir un carreau. Porte tr�s mal son nom.
	int nb_bomb; //nombre de bombes dans le "sac" du joueur
	bool bonus_nombre; //A-t-il le droit de poser plusieurs bombes en m�me temps.
    int bonus_portee; //Quelle est la port�e de ses bombes ?
    bool bonus_temps; //Ses bombes explosent-elles plus vite ?
    SDL_Surface *sprite; //Sprite du joueur
    SDL_Rect pos_pixel; //Position pr�cise du joueur en pixels.
    SDL_Rect clip[4][10]; //Partie du sprite � afficher d�coup� en morceau. Premiers crochets = direction du d�placement. Deuxi�me crochet = Frame de l'animation/
    int anim_frame; //Num�ro de la frame actuelle.
    int sens; //dans quel sens est-il tourn�. Ca va influencer l'affichage
    int anim_chrono; //Chrono de l'animation, reset au d�but de chaque d�placement.
	int touche[4]; //indique le nombre de fois qu'une bombe de ce joueur a touch� un autre joueur
	Bombe sac[2]; //le sac de bombe du joueur initialis� a sa taille maxiale possible
	int delai_bombe; //temps de posage de la bombe pr�c�dente, pour �viter qu'un joueur en pose deux d'un coup sans le vouloir.
	bool vivant;

}joueur;


typedef struct t_jeu{

    short nb_joueur; //Nb_joueurs qui ne sont pas des IA
    short nb_IA;
    bool mode_temps; //0 = avec des vies, 1 = avec du temps
    short niveau; // nombre de niveaux
    int duree; //Duree restante d'une partie.
    joueur* joueurtab; //Tableau dynamique des joueurs
	bool again;

}jeu;


typedef struct u_choix {
	bool quit;
	int mode; //0=vie 1= temps -1=quit
	int vies;
	int duree;
	int nb_joueur;
	int nb_ia;
}Choix;


//jeu* initjeu(jeu* lejeu, Choix liste_choix, int map_choisie);



#endif // STRUCT_H_INCLUDED
