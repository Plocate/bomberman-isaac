#include <stdlib.h>
#include <SDL/SDL.h>
#include <time.h>
#include <SDL/SDL_mixer.h>
#include "Struct.h"
#include "jeu.h"
#include "menu.h"
#include "map.h"


/*
Jeu bomberman par Bryan Vigee et Pierre-Antoine Locatelli
Date de cr�ation : 8/01/16
Derni�re modification : 14/01/16 (par Pierre-Antoine)
*/


int main(int argc, char** argv) {


	srand(time(NULL));

	SDL_Init(SDL_INIT_VIDEO);

    if(Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, MIX_DEFAULT_CHANNELS, 1024) == -1){ //Initialisation de l'API Mixer
        printf("%s", Mix_GetError());
    }

	int map_choisie, i;
	bool recommencer = TRUE;
	int** carte = NULL;
	jeu* lejeu = NULL;
	Choix set;
	SDL_Surface *ecran;
	SDL_Surface *icone = SDL_LoadBMP("Image/Icone.bmp"); //chargement et creation de l'icone
	SDL_SetColorKey(icone, SDL_SRCCOLORKEY, SDL_MapRGB(icone->format, 0, 255, 0));
	SDL_WM_SetIcon(icone, NULL);
	SDL_WM_SetCaption("BOMBERMAN Epic", NULL);//on nomme la fenetre
	ecran = SDL_SetVideoMode(LARGEUR_FENETRE, HAUTEUR_FENETRE, 32, SDL_HWSURFACE);//on cree la fenetre

	//Choix du joueur dans le menu
	while (recommencer)
	{
		Lancer_Menu(ecran, &set, &map_choisie);
		if (set.quit) {
			return 0;
		}
		//Initialisation des donn�es du jeu dans la structure "jeu".
		lejeu = malloc(sizeof(jeu));
		initjeu(lejeu, set, map_choisie);


		switch (map_choisie)
		{
		case 1:
			carte = ChargementMap1(carte);
			break;
		case 2:
			carte = ChargementMap2(carte);
			break;
		case 3:
			carte = ChargementMap3(carte);
			break;
		case 4:
			carte = ChargementMap4(carte);
			break;
		case 5:
			carte = ChargementMap5(carte);
			break;
		case 6:
			carte = randmap(carte, lejeu);
			break;
		default:
			break;
		}
		//Lancement du jeu.
		jouer(ecran, lejeu, carte);
		//fin du jeu
		if (lejeu-> again) end_game(ecran, lejeu);
		recommencer = lejeu->again;
		free(lejeu->joueurtab);
		free(lejeu);

		for (i = 0; i < NB_BLOC_X; i++) free(carte[i]);
		free(carte);

	}

	Mix_CloseAudio();
	SDL_Quit();
	return 0;
}

