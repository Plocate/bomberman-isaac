#include <stdlib.h>
#include <SDL/SDL.h>
#include <time.h>
#include <SDL_mixer.h>
#include "Struct.h"
#include "jeu.h"
#include "map.h"

void ajouter_danger_carte(Bombe labombe, int** carte, int carte_danger[][NB_BLOC_Y]){

    int i;
    //Dans le tableau des dangers, on donne le num�ro du joueur qui a pos� la bombe aux les zones o� la bombe va exploser
    if(labombe.num_joueur!=10 || carte_danger[labombe.posx][labombe.posy]==0) carte_danger[labombe.posx][labombe.posy] = labombe.num_joueur;
    i=1;
    while(i<=labombe.range && (carte[labombe.posx+i][labombe.posy]==VIDE || carte[labombe.posx+i][labombe.posy]==BOMBE || carte[labombe.posx+i][labombe.posy]>= MBOMB)){
            if(labombe.num_joueur!=10 || carte_danger[labombe.posx+i][labombe.posy]==0) carte_danger[labombe.posx+i][labombe.posy]=labombe.num_joueur;
            i++;
    }
    i=1;
    while(i<=labombe.range && (carte[labombe.posx-i][labombe.posy]==VIDE || carte[labombe.posx-i][labombe.posy]==BOMBE || carte[labombe.posx-i][labombe.posy]>= MBOMB)){
            if(labombe.num_joueur!=10 || carte_danger[labombe.posx-i][labombe.posy]==0) carte_danger[labombe.posx-i][labombe.posy]=labombe.num_joueur;
            i++;
    }
    i=1;
    while(i<=labombe.range && (carte[labombe.posx][labombe.posy+i]==VIDE || carte[labombe.posx][labombe.posy+i]==BOMBE || carte[labombe.posx][labombe.posy+i]>= MBOMB)){
            if(labombe.num_joueur!=10 || carte_danger[labombe.posx][labombe.posy+i]==0) carte_danger[labombe.posx][labombe.posy+i]=labombe.num_joueur;
            i++;
    }
    i=1;
    while(i<=labombe.range && (carte[labombe.posx][labombe.posy-i]==VIDE || carte[labombe.posx][labombe.posy-i]==BOMBE || carte[labombe.posx][labombe.posy-i]>= MBOMB)){
            if(labombe.num_joueur!=10 || carte_danger[labombe.posx][labombe.posy-i]==0) carte_danger[labombe.posx][labombe.posy-i]=labombe.num_joueur;
            i++;
    }
}

void enlever_danger_carte(Bombe labombe, int** carte, int carte_danger[][NB_BLOC_Y]){

    int i;
    //Dans le tableau des dangers, on remet � 0 les cases de la bombe qui vient d'exploser

    if(carte_danger[labombe.posx][labombe.posy]==labombe.num_joueur) carte_danger[labombe.posx][labombe.posy]=0;
    i=1;
    while(i<=labombe.range && (carte[labombe.posx+i][labombe.posy]==VIDE || carte[labombe.posx+i][labombe.posy]==BOMBE || carte[labombe.posx+i][labombe.posy]>= MBOMB)){
            if(carte_danger[labombe.posx+i][labombe.posy]==labombe.num_joueur) carte_danger[labombe.posx+i][labombe.posy]=0;
            i++;
    }
    i=1;
    while(i<=labombe.range && (carte[labombe.posx-i][labombe.posy]==VIDE || carte[labombe.posx-i][labombe.posy]==BOMBE || carte[labombe.posx-i][labombe.posy]>= MBOMB)){
            if(carte_danger[labombe.posx-i][labombe.posy]==labombe.num_joueur) carte_danger[labombe.posx-i][labombe.posy]=0;
            i++;
    }
    i=1;
    while(i<=labombe.range && (carte[labombe.posx][labombe.posy+i]==VIDE || carte[labombe.posx][labombe.posy+i]==BOMBE || carte[labombe.posx][labombe.posy+i]>= MBOMB)){
            if(carte_danger[labombe.posx][labombe.posy+i] == labombe.num_joueur) carte_danger[labombe.posx][labombe.posy+i]=0;
            i++;
    }
    i=1;
    while(i<=labombe.range && (carte[labombe.posx][labombe.posy-i]==VIDE || carte[labombe.posx][labombe.posy-i]==BOMBE || carte[labombe.posx][labombe.posy-i]>= MBOMB)){
            if(carte_danger[labombe.posx][labombe.posy-i] == labombe.num_joueur) carte_danger[labombe.posx][labombe.posy-i]=0;
            i++;
    }
}



int** randmap(int **tab, jeu* lejeu) { //G�n�ration d'un niveau al�atoire

	int i, j;
	float r;
    tab = (int**)malloc(sizeof(int*)*NB_BLOC_X);
	for (i = 0; i < NB_BLOC_X; i++) {
		tab[i] = (int*)malloc(sizeof(int)*NB_BLOC_Y);
	}
	//Les bords de la carte sont toujours des murs
	for (i = 0; i < NB_BLOC_Y; i++) {
		tab[0][i] = MUR;
		tab[NB_BLOC_X - 1][i] = MUR;
	}
	for (i = 1; i < NB_BLOC_X - 1; i++) {
		tab[i][0] = MUR;
		tab[i][NB_BLOC_Y - 1] = MUR;
	}


	for (i = 1; i < NB_BLOC_X - 1; i++) {
		for (j = 1; j < NB_BLOC_Y - 1; j++) {

			if (!(i > 2 || j>2)) tab[i][j] = VIDE; //Les camps de d�part des joueurs
			else if (lejeu->nb_joueur + lejeu->nb_IA >= 2 && !(i < NB_BLOC_X - 3 || j < NB_BLOC_Y - 3)) tab[i][j] = VIDE;
			else if (lejeu->nb_joueur + lejeu->nb_IA >= 3 && !(i > 2 || j < NB_BLOC_Y - 3)) tab[i][j] = VIDE;
			else if (lejeu->nb_joueur + lejeu->nb_IA >= 3 && !(i < NB_BLOC_X - 3 || j>2)) tab[i][j] = VIDE;
			else {
				//Chaque bloc restant a une chance d'�tre un rocher/un bloc.
				r = rand() % 100;
				if (r <= PROPORTION_ROCHERS) {
					tab[i][j] = ROCHER;
				}
				else if (r > PROPORTION_ROCHERS && r <= PROPORTION_BLOCS + PROPORTION_ROCHERS) {
					if (i == 1 || j == 1 || i == NB_BLOC_X - 2 || j == NB_BLOC_Y - 2) tab[i][j] = ROCHER;
					else tab[i][j] = BLOC; //On va interdire les blocs � la p�riph�rie de la map, pour �viter que le joueur soit bloqu�.
				}
				else {
					tab[i][j] = VIDE;
				}
			}
		}
	}


	return tab;
}

int **ChargementMap1(int **map) {
	int i, colone = 0;
	map = (int**)malloc(sizeof(int*)*NB_BLOC_X);
	for (i = 0; i < NB_BLOC_X; i++) {
		map[i] = (int*)malloc(sizeof(int)*NB_BLOC_Y);
	}
	for (i = 0; i < NB_BLOC_Y; i++) {
		map[0][i] = MUR;
		map[NB_BLOC_X - 1][i] = MUR;
	}
	for (i = 1; i < NB_BLOC_X - 1; i++) {
		map[i][0] = MUR;
		map[i][NB_BLOC_Y - 1] = MUR;
	}
	colone = 1;//1
	map[colone][1] = VIDE;
	map[colone][2] = VIDE;
	map[colone][3] = randomcailloux();
	map[colone][4] = randomcailloux();
	map[colone][5] = randomcailloux();
	map[colone][6] = randomcailloux();
	map[colone][7] = randomcailloux();
	map[colone][8] = randomcailloux();
	map[colone][9] = VIDE;
	map[colone][10] = VIDE;

	colone++;//2
	map[colone][1] = VIDE;
	map[colone][2] = BLOC;
	map[colone][3] = BLOC;
	map[colone][4] = randomcailloux();
	map[colone][5] = BLOC;
	map[colone][6] = BLOC;
	map[colone][7] = randomcailloux();
	map[colone][8] = BLOC;
	map[colone][9] = BLOC;
	map[colone][10] = VIDE;

	colone++;//3
	map[colone][1] = randomcailloux();
	map[colone][2] = randomcailloux();
	map[colone][3] = randomcailloux();
	map[colone][4] = randomcailloux();
	map[colone][5] = randomcailloux();
	map[colone][6] = randomcailloux();
	map[colone][7] = randomcailloux();
	map[colone][8] = randomcailloux();
	map[colone][9] = randomcailloux();
	map[colone][10] = randomcailloux();

	colone++;//4
	map[colone][1] = BLOC;
	map[colone][2] = randomcailloux();
	map[colone][3] = BLOC;
	map[colone][4] = BLOC;
	map[colone][5] = randomcailloux();
	map[colone][6] = BLOC;
	map[colone][7] = BLOC;
	map[colone][8] = BLOC;
	map[colone][9] = randomcailloux();
	map[colone][10] = BLOC;
	colone++;//5
	map[colone][1] = randomcailloux();
	map[colone][2] = randomcailloux();
	map[colone][3] = randomcailloux();
	map[colone][4] = randomcailloux();
	map[colone][5] = randomcailloux();
	map[colone][6] = randomcailloux();
	map[colone][7] = randomcailloux();
	map[colone][8] = randomcailloux();
	map[colone][9] = randomcailloux();
	map[colone][10] = randomcailloux();

	colone++;//6
	map[colone][1] = randomcailloux();
	map[colone][2] = BLOC;
	map[colone][3] = BLOC;
	map[colone][4] = randomcailloux();
	map[colone][5] = randomcailloux();
	map[colone][6] = randomcailloux();
	map[colone][7] = randomcailloux();
	map[colone][8] = BLOC;
	map[colone][9] = BLOC;
	map[colone][10] = randomcailloux();

	colone++;//7
	map[colone][1] = randomcailloux();
	map[colone][2] = randomcailloux();
	map[colone][3] = BLOC;
	map[colone][4] = randomcailloux();
	map[colone][5] = randomcailloux();
	map[colone][6] = randomcailloux();
	map[colone][7] = randomcailloux();
	map[colone][8] = BLOC;
	map[colone][9] = randomcailloux();
	map[colone][10] = randomcailloux();

	colone++;//8
	map[colone][1] = BLOC;
	map[colone][2] = randomcailloux();
	map[colone][3] = randomcailloux();
	map[colone][4] = randomcailloux();
	map[colone][5] = BLOC;
	map[colone][6] = BLOC;
	map[colone][7] = randomcailloux();
	map[colone][8] = randomcailloux();
	map[colone][9] = randomcailloux();
	map[colone][10] = BLOC;

	colone++;//9
	map[colone][1] = BLOC;
	map[colone][2] = randomcailloux();
	map[colone][3] = randomcailloux();
	map[colone][4] = randomcailloux();
	map[colone][5] = BLOC;
	map[colone][6] = BLOC;
	map[colone][7] = randomcailloux();
	map[colone][8] = randomcailloux();
	map[colone][9] = randomcailloux();
	map[colone][10] = BLOC;

	colone++;//10
	map[colone][1] = randomcailloux();
	map[colone][2] = randomcailloux();
	map[colone][3] = BLOC;
	map[colone][4] = randomcailloux();
	map[colone][5] = randomcailloux();
	map[colone][6] = randomcailloux();
	map[colone][7] = randomcailloux();
	map[colone][8] = BLOC;
	map[colone][9] = randomcailloux();
	map[colone][10] = randomcailloux();

	colone++;//11
	map[colone][1] = randomcailloux();
	map[colone][2] = BLOC;
	map[colone][3] = BLOC;
	map[colone][4] = randomcailloux();
	map[colone][5] = randomcailloux();
	map[colone][6] = BLOC;
	map[colone][7] = randomcailloux();
	map[colone][8] = BLOC;
	map[colone][9] = BLOC;
	map[colone][10] = randomcailloux();

	colone++;//12
	map[colone][1] = randomcailloux();
	map[colone][2] = randomcailloux();
	map[colone][3] = randomcailloux();
	map[colone][4] = randomcailloux();
	map[colone][5] = randomcailloux();
	map[colone][6] = randomcailloux();
	map[colone][7] = randomcailloux();
	map[colone][8] = randomcailloux();
	map[colone][9] = randomcailloux();
	map[colone][10] = randomcailloux();

	colone++;//13
	map[colone][1] = BLOC;
	map[colone][2] = randomcailloux();
	map[colone][3] = BLOC;
	map[colone][4] = BLOC;
	map[colone][5] = BLOC;
	map[colone][6] = randomcailloux();
	map[colone][7] = BLOC;
	map[colone][8] = BLOC;
	map[colone][9] = randomcailloux();
	map[colone][10] = BLOC;

	colone++;//14
	map[colone][1] = randomcailloux();
	map[colone][2] = randomcailloux();
	map[colone][3] = randomcailloux();
	map[colone][4] = randomcailloux();
	map[colone][5] = randomcailloux();
	map[colone][6] = randomcailloux();
	map[colone][7] = randomcailloux();
	map[colone][8] = randomcailloux();
	map[colone][9] = randomcailloux();
	map[colone][10] = randomcailloux();

	colone++;//15
	map[colone][1] = VIDE;
	map[colone][2] = BLOC;
	map[colone][3] = BLOC;
	map[colone][4] = randomcailloux();
	map[colone][5] = BLOC;
	map[colone][6] = BLOC;
	map[colone][7] = randomcailloux();
	map[colone][8] = BLOC;
	map[colone][9] = BLOC;
	map[colone][10] = VIDE;

	colone++;//16
	map[colone][1] = VIDE;
	map[colone][2] = VIDE;
	map[colone][3] = randomcailloux();
	map[colone][4] = randomcailloux();
	map[colone][5] = randomcailloux();
	map[colone][6] = randomcailloux();
	map[colone][7] = randomcailloux();
	map[colone][8] = randomcailloux();
	map[colone][9] = VIDE;
	map[colone][10] = VIDE;
	return map;
}

int **ChargementMap2(int ** map) {
	int i, colone = 0;
	map = (int**)malloc(sizeof(int*)*NB_BLOC_X);
	for (i = 0; i < NB_BLOC_X; i++) {
		map[i] = (int*)malloc(sizeof(int)*NB_BLOC_Y);
	}
	for (i = 0; i < NB_BLOC_Y; i++) {
		map[0][i] = MUR;
		map[NB_BLOC_X - 1][i] = MUR;
	}
	for (i = 1; i < NB_BLOC_X - 1; i++) {
		map[i][0] = MUR;
		map[i][NB_BLOC_Y - 1] = MUR;
	}

	colone++;//1
	map[colone][1] = VIDE;
	map[colone][2] = VIDE;
	map[colone][3] = randomcailloux();
	map[colone][4] = randomcailloux();
	map[colone][5] = randomcailloux();
	map[colone][6] = BLOC;
	map[colone][7] = randomcailloux();
	map[colone][8] = BLOC;
	map[colone][9] = VIDE;
	map[colone][10] = VIDE;

	colone++;//2
	map[colone][1] = VIDE;
	map[colone][2] = BLOC;
	map[colone][3] = randomcailloux();
	map[colone][4] = BLOC;
	map[colone][5] = randomcailloux();
	map[colone][6] = randomcailloux();
	map[colone][7] = randomcailloux();
	map[colone][8] = BLOC;
	map[colone][9] = BLOC;
	map[colone][10] = VIDE;

	colone++;//3
	map[colone][1] = BLOC;
	map[colone][2] = BLOC;
	map[colone][3] = randomcailloux();
	map[colone][4] = randomcailloux();
	map[colone][5] = BLOC;
	map[colone][6] = randomcailloux();
	map[colone][7] = BLOC;
	map[colone][8] = randomcailloux();
	map[colone][9] = randomcailloux();
	map[colone][10] = randomcailloux();

	colone++;//4
	map[colone][1] = randomcailloux();
	map[colone][2] = randomcailloux();
	map[colone][3] = randomcailloux();
	map[colone][4] = BLOC;
	map[colone][5] = randomcailloux();
	map[colone][6] = randomcailloux();
	map[colone][7] = randomcailloux();
	map[colone][8] = randomcailloux();
	map[colone][9] = BLOC;
	map[colone][10] = randomcailloux();

	colone++;//5
	map[colone][1] = randomcailloux();
	map[colone][2] = BLOC;
	map[colone][3] = BLOC;
	map[colone][4] = BLOC;
	map[colone][5] = randomcailloux();
	map[colone][6] = BLOC;
	map[colone][7] = randomcailloux();
	map[colone][8] = BLOC;
	map[colone][9] = randomcailloux();
	map[colone][10] = randomcailloux();

	colone++;//6
	map[colone][1] = randomcailloux();
	map[colone][2] = randomcailloux();
	map[colone][3] = BLOC;
	map[colone][4] = randomcailloux();
	map[colone][5] = randomcailloux();
	map[colone][6] = BLOC;
	map[colone][7] = randomcailloux();
	map[colone][8] = randomcailloux();
	map[colone][9] = randomcailloux();
	map[colone][10] = BLOC;

	colone++;//7
	map[colone][1] = BLOC;
	map[colone][2] = randomcailloux();
	map[colone][3] = randomcailloux();
	map[colone][4] = BLOC;
	map[colone][5] = randomcailloux();
	map[colone][6] = randomcailloux();
	map[colone][7] = randomcailloux();
	map[colone][8] = BLOC;
	map[colone][9] = randomcailloux();
	map[colone][10] = randomcailloux();

	colone++;//8
	map[colone][1] = BLOC;
	map[colone][2] = BLOC;
	map[colone][3] = randomcailloux();
	map[colone][4] = randomcailloux();
	map[colone][5] = randomcailloux();
	map[colone][6] = BLOC;
	map[colone][7] = randomcailloux();
	map[colone][8] = BLOC;
	map[colone][9] = randomcailloux();
	map[colone][10] = randomcailloux();

	colone++;//9
	map[colone][1] = BLOC;
	map[colone][2] = randomcailloux();
	map[colone][3] = randomcailloux();
	map[colone][4] = randomcailloux();
	map[colone][5] = BLOC;
	map[colone][6] = BLOC;
	map[colone][7] = randomcailloux();
	map[colone][8] = randomcailloux();
	map[colone][9] = randomcailloux();
	map[colone][10] = BLOC;

	colone++;//10
	map[colone][1] = randomcailloux();
	map[colone][2] = randomcailloux();
	map[colone][3] = BLOC;
	map[colone][4] = randomcailloux();
	map[colone][5] = randomcailloux();
	map[colone][6] = randomcailloux();
	map[colone][7] = BLOC;
	map[colone][8] = randomcailloux();
	map[colone][9] = randomcailloux();
	map[colone][10] = BLOC;

	colone++;//11
	map[colone][1] = BLOC;
	map[colone][2] = randomcailloux();
	map[colone][3] = randomcailloux();
	map[colone][4] = randomcailloux();
	map[colone][5] = BLOC;
	map[colone][6] = randomcailloux();
	map[colone][7] = randomcailloux();
	map[colone][8] = BLOC;
	map[colone][9] = randomcailloux();
	map[colone][10] = randomcailloux();

	colone++;//12
	map[colone][1] = randomcailloux();
	map[colone][2] = randomcailloux();
	map[colone][3] = BLOC;
	map[colone][4] = randomcailloux();
	map[colone][5] = BLOC;
	map[colone][6] = randomcailloux();
	map[colone][7] = BLOC;
	map[colone][8] = BLOC;
	map[colone][9] = BLOC;
	map[colone][10] = randomcailloux();

	colone++;//13
	map[colone][1] = randomcailloux();
	map[colone][2] = BLOC;
	map[colone][3] = randomcailloux();
	map[colone][4] = randomcailloux();
	map[colone][5] = randomcailloux();
	map[colone][6] = randomcailloux();
	map[colone][7] = BLOC;
	map[colone][8] = randomcailloux();
	map[colone][9] = randomcailloux();
	map[colone][10] = randomcailloux();

	colone++;//14
	map[colone][1] = randomcailloux();
	map[colone][2] = randomcailloux();
	map[colone][3] = randomcailloux();
	map[colone][4] = BLOC;
	map[colone][5] = randomcailloux();
	map[colone][6] = BLOC;
	map[colone][7] = randomcailloux();
	map[colone][8] = randomcailloux();
	map[colone][9] = BLOC;
	map[colone][10] = BLOC;

	colone++;//15
	map[colone][1] = VIDE;
	map[colone][2] = BLOC;
	map[colone][3] = BLOC;
	map[colone][4] = randomcailloux();
	map[colone][5] = randomcailloux();
	map[colone][6] = randomcailloux();
	map[colone][7] = BLOC;
	map[colone][8] = randomcailloux();
	map[colone][9] = BLOC;
	map[colone][10] = VIDE;

	colone++;//16
	map[colone][1] = VIDE;
	map[colone][2] = VIDE;
	map[colone][3] = BLOC;
	map[colone][4] = randomcailloux();
	map[colone][5] = BLOC;
	map[colone][6] = randomcailloux();
	map[colone][7] = randomcailloux();
	map[colone][8] = randomcailloux();
	map[colone][9] = VIDE;
	map[colone][10] = VIDE;
	return map;
}

int **ChargementMap3(int ** map) {
	int i, colone = 0;
	map = (int**)malloc(sizeof(int*)*NB_BLOC_X);
	for (i = 0; i < NB_BLOC_X; i++) {
		map[i] = (int*)malloc(sizeof(int)*NB_BLOC_Y);
	}
	for (i = 0; i < NB_BLOC_Y; i++) {
		map[0][i] = MUR;
		map[NB_BLOC_X - 1][i] = MUR;
	}
	for (i = 1; i < NB_BLOC_X - 1; i++) {
		map[i][0] = MUR;
		map[i][NB_BLOC_Y - 1] = MUR;
	}

	colone++;//1
	map[colone][1] = BLOC;
	map[colone][2] = VIDE;
	map[colone][3] = randomcailloux();
	map[colone][4] = randomcailloux();
	map[colone][5] = BLOC;
	map[colone][6] = BLOC;
	map[colone][7] = randomcailloux();
	map[colone][8] = randomcailloux();
	map[colone][9] = VIDE;
	map[colone][10] = BLOC;

	colone++;//2
	map[colone][1] = VIDE;
	map[colone][2] = VIDE;
	map[colone][3] = BLOC;
	map[colone][4] = randomcailloux();
	map[colone][5] = BLOC;
	map[colone][6] = BLOC;
	map[colone][7] = randomcailloux();
	map[colone][8] = BLOC;
	map[colone][9] = VIDE;
	map[colone][10] = VIDE;

	colone++;//3
	map[colone][1] = randomcailloux();
	map[colone][2] = BLOC;
	map[colone][3] = randomcailloux();
	map[colone][4] = randomcailloux();
	map[colone][5] = randomcailloux();
	map[colone][6] = randomcailloux();
	map[colone][7] = randomcailloux();
	map[colone][8] = randomcailloux();
	map[colone][9] = BLOC;
	map[colone][10] = randomcailloux();

	colone++;//4
	map[colone][1] = randomcailloux();
	map[colone][2] = randomcailloux();
	map[colone][3] = randomcailloux();
	map[colone][4] = BLOC;
	map[colone][5] = BLOC;
	map[colone][6] = BLOC;
	map[colone][7] = BLOC;
	map[colone][8] = randomcailloux();
	map[colone][9] = randomcailloux();
	map[colone][10] = randomcailloux();

	colone++;//5
	map[colone][1] = randomcailloux();
	map[colone][2] = BLOC;
	map[colone][3] = randomcailloux();
	map[colone][4] = randomcailloux();
	map[colone][5] = randomcailloux();
	map[colone][6] = randomcailloux();
	map[colone][7] = BLOC;
	map[colone][8] = randomcailloux();
	map[colone][9] = BLOC;
	map[colone][10] = BLOC;

	colone++;//6
	map[colone][1] = randomcailloux();
	map[colone][2] = randomcailloux();
	map[colone][3] = randomcailloux();
	map[colone][4] = BLOC;
	map[colone][5] = BLOC;
	map[colone][6] = randomcailloux();
	map[colone][7] = randomcailloux();
	map[colone][8] = randomcailloux();
	map[colone][9] = randomcailloux();
	map[colone][10] = randomcailloux();

	colone++;//7
	map[colone][1] = BLOC;
	map[colone][2] = randomcailloux();
	map[colone][3] = BLOC;
	map[colone][4] = randomcailloux();
	map[colone][5] = randomcailloux();
	map[colone][6] = randomcailloux();
	map[colone][7] = BLOC;
	map[colone][8] = BLOC;
	map[colone][9] = randomcailloux();
	map[colone][10] = BLOC;

	colone++;//8
	map[colone][1] = randomcailloux();
	map[colone][2] = randomcailloux();
	map[colone][3] = BLOC;
	map[colone][4] = randomcailloux();
	map[colone][5] = randomcailloux();
	map[colone][6] = BLOC;
	map[colone][7] = randomcailloux();
	map[colone][8] = randomcailloux();
	map[colone][9] = randomcailloux();
	map[colone][10] = randomcailloux();

	colone++;//9
	map[colone][1] = randomcailloux();
	map[colone][2] = randomcailloux();
	map[colone][3] = randomcailloux();
	map[colone][4] = randomcailloux();
	map[colone][5] = BLOC;
	map[colone][6] = randomcailloux();
	map[colone][7] = randomcailloux();
	map[colone][8] = BLOC;
	map[colone][9] = BLOC;
	map[colone][10] = BLOC;

	colone++;//10
	map[colone][1] = randomcailloux();
	map[colone][2] = BLOC;
	map[colone][3] = randomcailloux();
	map[colone][4] = BLOC;
	map[colone][5] = randomcailloux();
	map[colone][6] = randomcailloux();
	map[colone][7] = randomcailloux();
	map[colone][8] = randomcailloux();
	map[colone][9] = randomcailloux();
	map[colone][10] = BLOC;

	colone++;//11
	map[colone][1] = randomcailloux();
	map[colone][2] = randomcailloux();
	map[colone][3] = randomcailloux();
	map[colone][4] = randomcailloux();
	map[colone][5] = randomcailloux();
	map[colone][6] = randomcailloux();
	map[colone][7] = BLOC;
	map[colone][8] = BLOC;
	map[colone][9] = randomcailloux();
	map[colone][10] = randomcailloux();

	colone++;//12
	map[colone][1] = BLOC;
	map[colone][2] = BLOC;
	map[colone][3] = randomcailloux();
	map[colone][4] = randomcailloux();
	map[colone][5] = randomcailloux();
	map[colone][6] = BLOC;
	map[colone][7] = BLOC;
	map[colone][8] = randomcailloux();
	map[colone][9] = BLOC;
	map[colone][10] = BLOC;

	colone++;//13
	map[colone][1] = randomcailloux();
	map[colone][2] = randomcailloux();
	map[colone][3] = randomcailloux();
	map[colone][4] = BLOC;
	map[colone][5] = BLOC;
	map[colone][6] = BLOC;
	map[colone][7] = BLOC;
	map[colone][8] = randomcailloux();
	map[colone][9] = randomcailloux();
	map[colone][10] = randomcailloux();

	colone++;//14
	map[colone][1] = randomcailloux();
	map[colone][2] = BLOC;
	map[colone][3] = randomcailloux();
	map[colone][4] = randomcailloux();
	map[colone][5] = randomcailloux();
	map[colone][6] = randomcailloux();
	map[colone][7] = randomcailloux();
	map[colone][8] = randomcailloux();
	map[colone][9] = BLOC;
	map[colone][10] = randomcailloux();

	colone++;//15
	map[colone][1] = VIDE;
	map[colone][2] = VIDE;
	map[colone][3] = BLOC;
	map[colone][4] = randomcailloux();
	map[colone][5] = BLOC;
	map[colone][6] = BLOC;
	map[colone][7] = randomcailloux();
	map[colone][8] = BLOC;
	map[colone][9] = VIDE;
	map[colone][10] = VIDE;

	colone++;//16
	map[colone][1] = BLOC;
	map[colone][2] = VIDE;
	map[colone][3] = randomcailloux();
	map[colone][4] = randomcailloux();
	map[colone][5] = BLOC;
	map[colone][6] = BLOC;
	map[colone][7] = randomcailloux();
	map[colone][8] = randomcailloux();
	map[colone][9] = VIDE;
	map[colone][10] = BLOC;
	return map;
}

int **ChargementMap4(int ** map) {
	int i, colone = 0;
	map = (int**)malloc(sizeof(int*)*NB_BLOC_X);
	for (i = 0; i < NB_BLOC_X; i++) {
		map[i] = (int*)malloc(sizeof(int)*NB_BLOC_Y);
	}
	for (i = 0; i < NB_BLOC_Y; i++) {
		map[0][i] = MUR;
		map[NB_BLOC_X - 1][i] = MUR;
	}
	for (i = 1; i < NB_BLOC_X - 1; i++) {
		map[i][0] = MUR;
		map[i][NB_BLOC_Y - 1] = MUR;
	}

	colone++;//1
	map[colone][1] = BLOC;
	map[colone][2] = VIDE;
	map[colone][3] = randomcailloux();
	map[colone][4] = BLOC;
	map[colone][5] = BLOC;
	map[colone][6] = randomcailloux();
	map[colone][7] = randomcailloux();
	map[colone][8] = randomcailloux();
	map[colone][9] = VIDE;
	map[colone][10] = VIDE;

	colone++;//2
	map[colone][1] = VIDE;
	map[colone][2] = VIDE;
	map[colone][3] = randomcailloux();
	map[colone][4] = randomcailloux();
	map[colone][5] = randomcailloux();
	map[colone][6] = BLOC;
	map[colone][7] = randomcailloux();
	map[colone][8] = BLOC;
	map[colone][9] = BLOC;
	map[colone][10] = VIDE;

	colone++;//3
	map[colone][1] = randomcailloux();
	map[colone][2] = BLOC;
	map[colone][3] = BLOC;
	map[colone][4] = BLOC;
	map[colone][5] = randomcailloux();
	map[colone][6] = randomcailloux();
	map[colone][7] = randomcailloux();
	map[colone][8] = randomcailloux();
	map[colone][9] = BLOC;
	map[colone][10] = randomcailloux();

	colone++;//4
	map[colone][1] = randomcailloux();
	map[colone][2] = randomcailloux();
	map[colone][3] = BLOC;
	map[colone][4] = randomcailloux();
	map[colone][5] = randomcailloux();
	map[colone][6] = BLOC;
	map[colone][7] = BLOC;
	map[colone][8] = randomcailloux();
	map[colone][9] = randomcailloux();
	map[colone][10] = randomcailloux();

	colone++;//5
	map[colone][1] = BLOC;
	map[colone][2] = randomcailloux();
	map[colone][3] = randomcailloux();
	map[colone][4] = randomcailloux();
	map[colone][5] = BLOC;
	map[colone][6] = randomcailloux();
	map[colone][7] = randomcailloux();
	map[colone][8] = randomcailloux();
	map[colone][9] = BLOC;
	map[colone][10] = randomcailloux();

	colone++;//6
	map[colone][1] = randomcailloux();
	map[colone][2] = randomcailloux();
	map[colone][3] = BLOC;
	map[colone][4] = BLOC;
	map[colone][5] = randomcailloux();
	map[colone][6] = randomcailloux();
	map[colone][7] = BLOC;
	map[colone][8] = randomcailloux();
	map[colone][9] = randomcailloux();
	map[colone][10] = randomcailloux();

	colone++;//7
	map[colone][1] = BLOC;
	map[colone][2] = randomcailloux();
	map[colone][3] = randomcailloux();
	map[colone][4] = randomcailloux();
	map[colone][5] = randomcailloux();
	map[colone][6] = BLOC;
	map[colone][7] = BLOC;
	map[colone][8] = randomcailloux();
	map[colone][9] = BLOC;
	map[colone][10] = BLOC;

	colone++;//8
	map[colone][1] = randomcailloux();
	map[colone][2] = BLOC;
	map[colone][3] = randomcailloux();
	map[colone][4] = randomcailloux();
	map[colone][5] = randomcailloux();
	map[colone][6] = randomcailloux();
	map[colone][7] = randomcailloux();
	map[colone][8] = randomcailloux();
	map[colone][9] = randomcailloux();
	map[colone][10] = randomcailloux();

	colone++;//9
	map[colone][1] = randomcailloux();
	map[colone][2] = randomcailloux();
	map[colone][3] = BLOC;
	map[colone][4] = randomcailloux();
	map[colone][5] = BLOC;
	map[colone][6] = randomcailloux();
	map[colone][7] = BLOC;
	map[colone][8] = randomcailloux();
	map[colone][9] = BLOC;
	map[colone][10] = randomcailloux();

	colone++;//10
	map[colone][1] = BLOC;
	map[colone][2] = randomcailloux();
	map[colone][3] = randomcailloux();
	map[colone][4] = randomcailloux();
	map[colone][5] = randomcailloux();
	map[colone][6] = randomcailloux();
	map[colone][7] = randomcailloux();
	map[colone][8] = randomcailloux();
	map[colone][9] = randomcailloux();
	map[colone][10] = randomcailloux();

	colone++;//11
	map[colone][1] = randomcailloux();
	map[colone][2] = randomcailloux();
	map[colone][3] = BLOC;
	map[colone][4] = randomcailloux();
	map[colone][5] = BLOC;
	map[colone][6] = randomcailloux();
	map[colone][7] = randomcailloux();
	map[colone][8] = BLOC;
	map[colone][9] = randomcailloux();
	map[colone][10] = BLOC;

	colone++;//12
	map[colone][1] = randomcailloux();
	map[colone][2] = randomcailloux();
	map[colone][3] = randomcailloux();
	map[colone][4] = randomcailloux();
	map[colone][5] = randomcailloux();
	map[colone][6] = randomcailloux();
	map[colone][7] = randomcailloux();
	map[colone][8] = randomcailloux();
	map[colone][9] = randomcailloux();
	map[colone][10] = BLOC;

	colone++;//13
	map[colone][1] = randomcailloux();
	map[colone][2] = BLOC;
	map[colone][3] = randomcailloux();
	map[colone][4] = BLOC;
	map[colone][5] = randomcailloux();
	map[colone][6] = randomcailloux();
	map[colone][7] = randomcailloux();
	map[colone][8] = BLOC;
	map[colone][9] = randomcailloux();
	map[colone][10] = randomcailloux();

	colone++;//14
	map[colone][1] = randomcailloux();
	map[colone][2] = BLOC;
	map[colone][3] = randomcailloux();
	map[colone][4] = randomcailloux();
	map[colone][5] = BLOC;
	map[colone][6] = randomcailloux();
	map[colone][7] = BLOC;
	map[colone][8] = BLOC;
	map[colone][9] = BLOC;
	map[colone][10] = randomcailloux();

	colone++;//15
	map[colone][1] = VIDE;
	map[colone][2] = BLOC;
	map[colone][3] = BLOC;
	map[colone][4] = randomcailloux();
	map[colone][5] = BLOC;
	map[colone][6] = randomcailloux();
	map[colone][7] = BLOC;
	map[colone][8] = randomcailloux();
	map[colone][9] = VIDE;
	map[colone][10] = VIDE;

	colone++;//16
	map[colone][1] = VIDE;
	map[colone][2] = VIDE;
	map[colone][3] = randomcailloux();
	map[colone][4] = randomcailloux();
	map[colone][5] = randomcailloux();
	map[colone][6] = randomcailloux();
	map[colone][7] = randomcailloux();
	map[colone][8] = randomcailloux();
	map[colone][9] = VIDE;
	map[colone][10] = BLOC;

	return map;
}

int **ChargementMap5(int ** map) {
	int i, colone = 0;
	map = (int**)malloc(sizeof(int*)*NB_BLOC_X);
	for (i = 0; i < NB_BLOC_X; i++) {
		map[i] = (int*)malloc(sizeof(int)*NB_BLOC_Y);
	}
	for (i = 0; i < NB_BLOC_Y; i++) {
		map[0][i] = MUR;
		map[NB_BLOC_X - 1][i] = MUR;
	}
	for (i = 1; i < NB_BLOC_X - 1; i++) {
		map[i][0] = MUR;
		map[i][NB_BLOC_Y - 1] = MUR;
	}

	colone++;//1
	map[colone][1] = randomcailloux();
	map[colone][2] = randomcailloux();
	map[colone][3] = randomcailloux();
	map[colone][4] = BLOC;
	map[colone][5] = randomcailloux();
	map[colone][6] = randomcailloux();
	map[colone][7] = randomcailloux();
	map[colone][8] = randomcailloux();
	map[colone][9] = randomcailloux();
	map[colone][10] = randomcailloux();

	colone++;//2
	map[colone][1] = randomcailloux();
	map[colone][2] = BLOC;
	map[colone][3] = BLOC;
	map[colone][4] = randomcailloux();
	map[colone][5] = randomcailloux();
	map[colone][6] = randomcailloux();
	map[colone][7] = BLOC;
	map[colone][8] = BLOC;
	map[colone][9] = randomcailloux();
	map[colone][10] = BLOC;

	colone++;//3
	map[colone][1] = randomcailloux();
	map[colone][2] = randomcailloux();
	map[colone][3] = randomcailloux();
	map[colone][4] = BLOC;
	map[colone][5] = randomcailloux();
	map[colone][6] = randomcailloux();
	map[colone][7] = BLOC;
	map[colone][8] = randomcailloux();
	map[colone][9] = randomcailloux();
	map[colone][10] = randomcailloux();

	colone++;//4
	map[colone][1] = randomcailloux();
	map[colone][2] = BLOC;
	map[colone][3] = randomcailloux();
	map[colone][4] = randomcailloux();
	map[colone][5] = randomcailloux();
	map[colone][6] = randomcailloux();
	map[colone][7] = randomcailloux();
	map[colone][8] = randomcailloux();
	map[colone][9] = BLOC;
	map[colone][10] = randomcailloux();

	colone++;//5
	map[colone][1] = randomcailloux();
	map[colone][2] = randomcailloux();
	map[colone][3] = BLOC;
	map[colone][4] = VIDE;
	map[colone][5] = BLOC;
	map[colone][6] = BLOC;
	map[colone][7] = VIDE;
	map[colone][8] = BLOC;
	map[colone][9] = randomcailloux();
	map[colone][10] = randomcailloux();

	colone++;//6
	map[colone][1] = BLOC;
	map[colone][2] = randomcailloux();
	map[colone][3] = VIDE;
	map[colone][4] = VIDE;
	map[colone][5] = BLOC;
	map[colone][6] = BLOC;
	map[colone][7] = VIDE;
	map[colone][8] = VIDE;
	map[colone][9] = randomcailloux();
	map[colone][10] = BLOC;

	colone++;//7
	map[colone][1] = randomcailloux();
	map[colone][2] = randomcailloux();
	map[colone][3] = BLOC;
	map[colone][4] = BLOC;
	map[colone][5] = BLOC;
	map[colone][6] = BLOC;
	map[colone][7] = BLOC;
	map[colone][8] = BLOC;
	map[colone][9] = randomcailloux();
	map[colone][10] = randomcailloux();

	colone++;//8
	map[colone][1] = randomcailloux();
	map[colone][2] = randomcailloux();
	map[colone][3] = BLOC;
	map[colone][4] = BLOC;
	map[colone][5] = BLOC;
	map[colone][6] = BLOC;
	map[colone][7] = BLOC;
	map[colone][8] = BLOC;
	map[colone][9] = randomcailloux();
	map[colone][10] = randomcailloux();

	colone++;//9
	map[colone][1] = BLOC;
	map[colone][2] = randomcailloux();
	map[colone][3] = VIDE;
	map[colone][4] = VIDE;
	map[colone][5] = BLOC;
	map[colone][6] = BLOC;
	map[colone][7] = VIDE;
	map[colone][8] = VIDE;
	map[colone][9] = randomcailloux();
	map[colone][10] = BLOC;

	colone++;//10
	map[colone][1] = randomcailloux();
	map[colone][2] = randomcailloux();
	map[colone][3] = BLOC;
	map[colone][4] = VIDE;
	map[colone][5] = BLOC;
	map[colone][6] = BLOC;
	map[colone][7] = VIDE;
	map[colone][8] = BLOC;
	map[colone][9] = randomcailloux();
	map[colone][10] = randomcailloux();

	colone++;//11
	map[colone][1] = randomcailloux();
	map[colone][2] = BLOC;
	map[colone][3] = randomcailloux();
	map[colone][4] = randomcailloux();
	map[colone][5] = randomcailloux();
	map[colone][6] = randomcailloux();
	map[colone][7] = randomcailloux();
	map[colone][8] = randomcailloux();
	map[colone][9] = BLOC;
	map[colone][10] = randomcailloux();

	colone++;//12
	map[colone][1] = randomcailloux();
	map[colone][2] = randomcailloux();
	map[colone][3] = randomcailloux();
	map[colone][4] = BLOC;
	map[colone][5] = randomcailloux();
	map[colone][6] = randomcailloux();
	map[colone][7] = BLOC;
	map[colone][8] = randomcailloux();
	map[colone][9] = randomcailloux();
	map[colone][10] = randomcailloux();

	colone++;//13
	map[colone][1] = BLOC;
	map[colone][2] = randomcailloux();
	map[colone][3] = BLOC;
	map[colone][4] = randomcailloux();
	map[colone][5] = BLOC;
	map[colone][6] = randomcailloux();
	map[colone][7] = randomcailloux();
	map[colone][8] = BLOC;
	map[colone][9] = randomcailloux();
	map[colone][10] = randomcailloux();

	colone++;//14
	map[colone][1] = randomcailloux();
	map[colone][2] = randomcailloux();
	map[colone][3] = randomcailloux();
	map[colone][4] = randomcailloux();
	map[colone][5] = randomcailloux();
	map[colone][6] = randomcailloux();
	map[colone][7] = randomcailloux();
	map[colone][8] = randomcailloux();
	map[colone][9] = randomcailloux();
	map[colone][10] = BLOC;

	colone++;//15
	map[colone][1] = randomcailloux();
	map[colone][2] = BLOC;
	map[colone][3] = randomcailloux();
	map[colone][4] = BLOC;
	map[colone][5] = BLOC;
	map[colone][6] = BLOC;
	map[colone][7] = randomcailloux();
	map[colone][8] = BLOC;
	map[colone][9] = randomcailloux();
	map[colone][10] = randomcailloux();

	colone++;//16
	map[colone][1] = randomcailloux();
	map[colone][2] = randomcailloux();
	map[colone][3] = randomcailloux();
	map[colone][4] = randomcailloux();
	map[colone][5] = randomcailloux();
	map[colone][6] = randomcailloux();
	map[colone][7] = randomcailloux();
	map[colone][8] = randomcailloux();
	map[colone][9] = randomcailloux();
	map[colone][10] = BLOC;

	return map;
}

int randomcailloux() {
	int ret;
	ret = rand() % 100;
	if (ret <= 85) {
		return 1;
	}
	else return 0;
}
