#ifndef JEU_H_INCLUDED
#define JEU_H_INCLUDED
#include "Struct.h"

#define SPRITE_X 21 //Largeur des sprite de bomberman
#define SPRITE_Y 32 //Hauteur des sprite de bomberman
#define BOMBE_TAILLE_X 21 //Largeur des sprites de bombes
#define BOMBE_TAILLE_Y 26 //Hauteur des sprite de bombes
#define COMPTE_A_REBOURS_INIT 2000
#define DELAI_BOMBE 200 //Temps entre le posage de deux bombe par un même joueur, pour éviter qu'il en pose 2 sans le vouloir.
#define MINUTE 60000
#define DECEDE 0
#define VIVANT 1

//int** randniveau(int **tab, jeu* lejeu);
void UpdateEvent(Input *in);
void jouer(SDL_Surface* ecran, jeu* lejeu, int** carte);
void commence_deplacement(joueur* lejoueur, int** carte, int direction);
void deplacement(joueur* lejoueur);
void initjeu(jeu* lejeu, Choix liste_choix, int map_choisie);
void posebomb(int** map, joueur *plyr, Bombe *bomb, int carte_danger[][NB_BLOC_Y]);
void Boom(int** carte, Bombe *bomb, joueur *plyr, SDL_Surface *ecran, SDL_Surface *BOOM, joueur *menaces, int carte_danger[][NB_BLOC_Y], int nb_persos, Mix_Chunk *bruit);
int ange_de_la_mort(joueur *candidat);
void end_game(SDL_Surface *screen, jeu *game);
int who_won(jeu *game, int *winner, int *comp);
int spawn_bonus();
int which_bonus();

void mouvement_IA(joueur* IA, int carte_danger[][NB_BLOC_Y], int** carte);
void IA_poser_bombe(joueur* IA, jeu* lejeu, int carte_danger[][NB_BLOC_Y], int** carte, int* b);
int IA_pathfinding(int x, int y, int distance, int* min, int carte_danger[][NB_BLOC_Y], int tab_chemin[][NB_BLOC_Y]);




#endif // JEU_H_INCLUDED
