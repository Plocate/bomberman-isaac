#include <stdlib.h>
#include <SDL/SDL.h>
#include <time.h>
#include <SDL_mixer.h>
#include "Struct.h"
#include "jeu.h"
#include "map.h"
#include "menu.h"


int spawn_bonus() {
    //Donne 20% de chance à un bonus d'apparaître
	int bon;
	bon = rand() % 100;
	if (bon <= 20) return 1;
	return 0;
}

int which_bonus() {
	int lapata;
	lapata = rand() % 100;
	if (lapata < 10) return MBOMB;
	if (lapata < 40) return RANGE;
	if (lapata < 50) return MHEART;
	if (lapata < 70) return TIME;
	else return SPEED;
}

void UpdateEvent(Input *in) {
	SDL_Event event;
	while (SDL_PollEvent(&event)) {
		switch (event.type)
		{
		case SDL_KEYDOWN:
			in->key[event.key.keysym.sym] = 1;
			break;
		case SDL_KEYUP:
			in->key[event.key.keysym.sym] = 0;
			break;
		default:
			break;
		}
	}
}

void posebomb(int** map, joueur *plyr, Bombe *bomb, int carte_danger[][NB_BLOC_Y]) {
    if(map[plyr->pos_x][plyr->pos_y] == BOMBE) return;
	map[plyr->pos_x][plyr->pos_y] = BOMBE;
	bomb->temps = SDL_GetTicks();
	bomb->range = plyr->bonus_portee + 1;
	bomb->posx = plyr->pos_x;
	bomb->posy = plyr->pos_y;
	bomb->pos.x = bomb->posx*TAILLE_BLOC;
	bomb->pos.y = bomb->posy*TAILLE_BLOC;
	bomb->posee = TRUE;
	plyr->nb_bomb--;
	plyr->delai_bombe = SDL_GetTicks();
	ajouter_danger_carte(*bomb, map, carte_danger); //Utile pour l'IA.
}

void Boom(int** carte, Bombe *bomb, joueur *plyr, SDL_Surface *ecran, SDL_Surface *BOOM, joueur *menaces, int carte_danger[][NB_BLOC_Y], int nb_persos, Mix_Chunk *bruit) {
	SDL_Rect boum;
	int i, bon, temp, j = 0, k = 0, l = 0, m = 0;
	carte[bomb->posx][bomb->posy] = VIDE;
	boum.x = 68;
	boum.y = 68;
	boum.h = 35;
	boum.w = 34;
	temp = bomb->pos.x;

    if(Mix_Playing(0)){
        Mix_HaltChannel(0); //Si un son d'explosion jouait déjà on l'interromp
    }
	Mix_PlayChannel(0, bruit, 0);

	SDL_BlitSurface(BOOM, &boum, ecran, &bomb->pos);
	SDL_Flip(ecran);


	//Explosion vers la droite
	while (j <= bomb->range) {
        //Destrcution de cailloux
		if (carte[bomb->posx + j][bomb->posy] == ROCHER) {
			bon = spawn_bonus();
			if (bon == 0) carte[bomb->posx + j][bomb->posy] = VIDE;
			else carte[bomb->posx + j][bomb->posy] = which_bonus();
			j++;
			break;
		}
		else if (carte[bomb->posx + j][bomb->posy] == BOMBE){
            for(i=0; i<nb_persos; i++){
            //Réaction en chaine
             if(menaces[i].sac[0].posee && menaces[i].sac[0].posx == bomb->posx + j && menaces[i].sac[0].posy == bomb->posy) Boom(carte, &menaces[i].sac[0], &menaces[i], ecran, BOOM, menaces, carte_danger, nb_persos, bruit);
             if(menaces[i].sac[1].posee && menaces[i].sac[1].posx == bomb->posx + j && menaces[i].sac[1].posy == bomb->posy) Boom(carte, &menaces[i].sac[1], &menaces[i], ecran, BOOM, menaces, carte_danger, nb_persos, bruit);
            }
		}
        //On n'arrête au premier mur ou bloc
		else if (carte[bomb->posx + j][bomb->posy] == MUR || carte[bomb->posx + j][bomb->posy] == BLOC) break;
		j++;
	}

	//Affichage de l'explosion
	for (i = 1; i < j; i++) {
		bomb->pos.x += TAILLE_BLOC;
		if (i != j - 1) {
			boum.x = 102;
			bomb->pos.x += i;
			SDL_BlitSurface(BOOM, &boum, ecran, &bomb->pos);
		}
		else {
			boum.x = 136;
			SDL_BlitSurface(BOOM, &boum, ecran, &bomb->pos);
		}
	}
	bomb->pos.x = temp;

	//Par la suite, on va refaire de même dans les 4 directions restantes.
	//Ici c'est gauche
	while (k <= bomb->range) {
		if (carte[bomb->posx - k][bomb->posy] == ROCHER) {
			bon = spawn_bonus();
			if (bon == 0) carte[bomb->posx - k][bomb->posy] = VIDE;
			else carte[bomb->posx - k][bomb->posy] = which_bonus();
			k++;
			break;
		}
		else if (carte[bomb->posx -k][bomb->posy] == BOMBE){
            for(i=0; i<nb_persos; i++){
             if(menaces[i].sac[0].posee && menaces[i].sac[0].posx == bomb->posx -k && menaces[i].sac[0].posy == bomb->posy) Boom(carte, &menaces[i].sac[0], &menaces[i], ecran, BOOM, menaces, carte_danger, nb_persos, bruit);
             if(menaces[i].sac[1].posee && menaces[i].sac[1].posx == bomb->posx -k && menaces[i].sac[1].posy == bomb->posy) Boom(carte, &menaces[i].sac[1], &menaces[i], ecran, BOOM, menaces, carte_danger, nb_persos, bruit);
            }
		}
		else if (carte[bomb->posx - k][bomb->posy] == MUR || carte[bomb->posx - k][bomb->posy] == BLOC) break;
		k++;
	}
	for (i = 1; i < k; i++) {
		bomb->pos.x -= TAILLE_BLOC;
		if (i != k - 1) {
			boum.x = 34;
			bomb->pos.x--;
			SDL_BlitSurface(BOOM, &boum, ecran, &bomb->pos);
		}
		else {
			boum.x = 0;
			SDL_BlitSurface(BOOM, &boum, ecran, &bomb->pos);
		}
	}


	bomb->pos.x = temp;
	temp = bomb->pos.y;
	boum.x = 68;

    //explosion vers le bas, idem
	while (l <= bomb->range) {
		if (carte[bomb->posx][bomb->posy + l] == ROCHER) {
			bon = spawn_bonus();
			if (bon == 0) carte[bomb->posx][bomb->posy + l] = VIDE;
			else carte[bomb->posx][bomb->posy + l] = which_bonus();
			l++;
			break;
		}
		else if (carte[bomb->posx][bomb->posy +l] == BOMBE){
            for(i=0; i<nb_persos; i++){
             if(menaces[i].sac[0].posee && menaces[i].sac[0].posx == bomb->posx && menaces[i].sac[0].posy == bomb->posy+l) Boom(carte, &menaces[i].sac[0], &menaces[i], ecran, BOOM, menaces, carte_danger, nb_persos, bruit);
             if(menaces[i].sac[1].posee && menaces[i].sac[1].posx == bomb->posx && menaces[i].sac[1].posy == bomb->posy+l) Boom(carte, &menaces[i].sac[1], &menaces[i], ecran, BOOM, menaces, carte_danger, nb_persos, bruit);
            }
		}
		else if (carte[bomb->posx][bomb->posy + l] == MUR || carte[bomb->posx][bomb->posy + l] == BLOC) break;
		l++;
	}
	for (i = 1; i < l; i++) {
		bomb->pos.y += TAILLE_BLOC;
		if (i != l - 1) {
			boum.y = 102;
			bomb->pos.y += i;
			SDL_BlitSurface(BOOM, &boum, ecran, &bomb->pos);
		}
		else {
			boum.y = 136;
			SDL_BlitSurface(BOOM, &boum, ecran, &bomb->pos);
		}
	}
	bomb->pos.y = temp;


	//explosion vers le haut, idem.
	while (m <= bomb->range) {
		if (carte[bomb->posx][bomb->posy - m] == ROCHER) {
			bon = spawn_bonus();
			if (bon == 0) carte[bomb->posx][bomb->posy - m] = VIDE;
			else  carte[bomb->posx][bomb->posy - m] = which_bonus();
			m++;
			break;
		}
		else if (carte[bomb->posx][bomb->posy-m] == BOMBE){
            for(i=0; i<nb_persos; i++){
             if(menaces[i].sac[0].posee && menaces[i].sac[0].posx == bomb->posx && menaces[i].sac[0].posy== bomb->posy -m) Boom(carte, &menaces[i].sac[0], &menaces[i], ecran, BOOM, menaces, carte_danger, nb_persos, bruit);
             if(menaces[i].sac[1].posee && menaces[i].sac[1].posx == bomb->posx && menaces[i].sac[1].posy== bomb->posy -m) Boom(carte, &menaces[i].sac[1], &menaces[i], ecran, BOOM, menaces, carte_danger, nb_persos, bruit);
            }
		}
		else if (carte[bomb->posx][bomb->posy - m] == MUR || carte[bomb->posx][bomb->posy - m] == BLOC) break;
		m++;
	}
	for (i = 1; i < m; i++) {
		bomb->pos.y -= TAILLE_BLOC;
		if (i != m - 1) {
			boum.y = 34;
			bomb->pos.y--;
			SDL_BlitSurface(BOOM, &boum, ecran, &bomb->pos);
		}
		else {
			boum.y = 0;
			SDL_BlitSurface(BOOM, &boum, ecran, &bomb->pos);
		}
	}
	bomb->pos.y = temp;

	bomb->posee = FALSE;

	SDL_Flip(ecran);
	SDL_Delay(100);


	//On fait descendre les vies et augmenter le score.
	for (i = 0; i <nb_persos; i++)
	{
		if (menaces[i].vivant) {
			if (bomb->posy == menaces[i].pos_y && bomb->posx == menaces[i].pos_x) {
				menaces[i].vie--;
				if(i+1 != plyr->num) plyr->touche[i] += 1;
			}
			else {
				if (bomb->posx - k < menaces[i].pos_x&&bomb->posx + j > menaces[i].pos_x) {
					if (bomb->posy == menaces[i].pos_y) {
						menaces[i].vie--;
						if(i+1 != plyr->num) plyr->touche[i] += 1;
					}
				}
				if (bomb->posy - m < menaces[i].pos_y&&bomb->posy + l > menaces[i].pos_y) {
					if (bomb->posx == menaces[i].pos_x) {
						menaces[i].vie--;
						if(i+1 != plyr->num) plyr->touche[i] += 1;
					}
				}
			}

		}
	}

    //On redonne sa bombe au joueur.
	if (!plyr->bonus_nombre) plyr->nb_bomb = 1;
	else {
		if (plyr->nb_bomb == 1) plyr->nb_bomb = 2;
		else if (plyr->nb_bomb <= 0) plyr->nb_bomb = 1;
	}
	enlever_danger_carte(*bomb, carte, carte_danger);

}


int ange_de_la_mort(joueur *candidat) {
	if (candidat->vie <= 0)
	{
		candidat->vivant = FALSE;
		return DECEDE; //=0
	}
	return VIVANT; //=1
}


//Déroulement de la partie.
void jouer(SDL_Surface* ecran, jeu* lejeu, int** carte) {
	int i, j, bada, b = 0, tps_debut, check, tps, survivants = lejeu->nb_IA + lejeu->nb_joueur;
	bool continuer;
	SDL_Surface *mur = NULL, *coin = NULL, *bloc = NULL, *sol = NULL, *rock = NULL, *bomb = NULL, *boom = NULL;
	SDL_Surface *mbomb, *drop, *speed, *mheart, *range;
	Mix_Music *musique;
	Mix_Chunk *bruit, *sonbonus;
	Mix_AllocateChannels(2);
	SDL_Rect murclip[4], coinclip[4], position;
	int carte_danger[NB_BLOC_X][NB_BLOC_Y];
	Input in;
	for(i=0; i<NB_BLOC_X; i++){
        for(j=0; j<NB_BLOC_Y; j++){
            carte_danger[i][j] = 0;
        }
    }

	tps_debut = SDL_GetTicks();
	//On charge les 4 murs et ont découpe l'image en plusieurs parties
	mur = SDL_LoadBMP("Image/mur.bmp");
	for (i = 0; i < 4; i++) {
		murclip[i].x = TAILLE_BLOC*i;
		murclip[i].y = 0;
		murclip[i].w = TAILLE_BLOC;
		murclip[i].h = TAILLE_BLOC;
	}

	//On charge les 4 coins et on découpe l'image en 4 parties.
	//HAUT = en haut à gauche, BAS = en bas à droite, GAUCHE = en bas à gauche et DROITE = en haut à droite
	coin = SDL_LoadBMP("Image/coin.bmp");
	for (i = 0; i < 4; i++) {
		coinclip[i].x = TAILLE_BLOC*i;
		coinclip[i].y = 0;
		coinclip[i].w = TAILLE_BLOC;
		coinclip[i].h = TAILLE_BLOC;
	}

	//On charge les autres blocs.
	bloc = SDL_LoadBMP("Image/bloc.bmp");
	sol = SDL_LoadBMP("Image/solcarre.bmp");
	rock = SDL_LoadBMP("Image/Rock.bmp");
	bomb = SDL_LoadBMP("Image/bomb.bmp");
	boom = SDL_LoadBMP("Image/BOOOM.bmp");
	SDL_SetColorKey(bomb, SDL_SRCCOLORKEY, SDL_MapRGB(bomb->format, 0, 255, 0));
	SDL_SetColorKey(rock, SDL_SRCCOLORKEY, SDL_MapRGB(rock->format, 0, 255, 0));
	SDL_SetColorKey(boom, SDL_SRCCOLORKEY, SDL_MapRGB(boom->format, 0, 255, 0));

	mbomb = SDL_LoadBMP("Image/bomb+.bmp");
	drop = SDL_LoadBMP("Image/countdown.bmp");
	range = SDL_LoadBMP("Image/range+.bmp");
	speed = SDL_LoadBMP("Image/roller.bmp");
	mheart = SDL_LoadBMP("Image/Heart+.bmp");

	SDL_SetColorKey(mbomb, SDL_SRCCOLORKEY, SDL_MapRGB(mbomb->format, 0, 255, 0));
	SDL_SetColorKey(drop, SDL_SRCCOLORKEY, SDL_MapRGB(drop->format, 0, 255, 0));
	SDL_SetColorKey(range, SDL_SRCCOLORKEY, SDL_MapRGB(range->format, 0, 255, 0));
	SDL_SetColorKey(speed, SDL_SRCCOLORKEY, SDL_MapRGB(speed->format, 0, 255, 0));
	SDL_SetColorKey(mheart, SDL_SRCCOLORKEY, SDL_MapRGB(mheart->format, 0, 255, 0));

    //On charge les sons et musiques
    musique = Mix_LoadMUS("Son/Wrath.wav");
    bruit = Mix_LoadWAV("Son/explo.wav");
    sonbonus = Mix_LoadWAV("Son/Sonbonus.wav");
    Mix_VolumeMusic(MIX_MAX_VOLUME/8);
    Mix_VolumeChunk(bruit, MIX_MAX_VOLUME/8);
    Mix_VolumeChunk(sonbonus, MIX_MAX_VOLUME);
    Mix_PlayMusic(musique, -1);


	continuer = TRUE;
	memset(&in, 0, sizeof(in));


    while(!in.key[SDLK_ESCAPE] && continuer){

        UpdateEvent(&in);

        //Inputs du joueur 1
        if (lejeu->joueurtab[0].vivant) {
            //déplacements
            if (in.key[SDLK_w] || in.key[SDLK_z]) commence_deplacement(lejeu->joueurtab, carte, HAUT); //Windows préfère le qwerty, Linux préfère l'azerty. Dans le doute, les deux sont possibles.

            if (in.key[SDLK_s]) commence_deplacement(lejeu->joueurtab, carte, BAS);

            if (in.key[SDLK_a] || in.key[SDLK_q]) commence_deplacement(lejeu->joueurtab, carte, GAUCHE);

            if (in.key[SDLK_d]) commence_deplacement(lejeu->joueurtab, carte, DROITE);
        }
        if (in.key[SDLK_LSHIFT]) {
            //Il pose une bombe.
            tps = SDL_GetTicks();
            if (lejeu->joueurtab[0].vivant && tps-lejeu->joueurtab[0].delai_bombe > DELAI_BOMBE) {
                if (!lejeu->joueurtab[0].bonus_nombre && lejeu->joueurtab[0].nb_bomb == 1)	posebomb(carte, &lejeu->joueurtab[0], &lejeu->joueurtab[0].sac[0], carte_danger);
                else if (lejeu->joueurtab[0].bonus_nombre) {
                    if ((lejeu->joueurtab[0].nb_bomb ==1 || lejeu->joueurtab[0].nb_bomb == 2) && carte[lejeu->joueurtab[0].pos_x][lejeu->joueurtab[0].pos_y] == VIDE) {
                        if(lejeu->joueurtab[0].sac[b%2].posee) b++;
                        posebomb(carte, &lejeu->joueurtab[0], &lejeu->joueurtab[0].sac[b % 2], carte_danger);
                    }
                }
            }

        }

        //Inputs du joueur 2
        if (lejeu->nb_joueur == 2 && lejeu->joueurtab[1].vivant) {
            //Déplacement
            if (in.key[SDLK_UP]) commence_deplacement(lejeu->joueurtab + 1, carte, HAUT);

            if (in.key[SDLK_DOWN]) commence_deplacement(lejeu->joueurtab + 1, carte, BAS);

            if (in.key[SDLK_LEFT]) commence_deplacement(lejeu->joueurtab + 1, carte, GAUCHE);

            if (in.key[SDLK_RIGHT]) commence_deplacement(lejeu->joueurtab + 1, carte, DROITE);

            if (in.key[SDLK_RSHIFT]) {
            //Il pose une bombe
                tps = SDL_GetTicks();
                if(tps-lejeu->joueurtab[1].delai_bombe > DELAI_BOMBE){
                    if (!lejeu->joueurtab[1].bonus_nombre && lejeu->joueurtab[1].nb_bomb == 1)	posebomb(carte, &lejeu->joueurtab[1], &lejeu->joueurtab[1].sac[0], carte_danger);
                    else if (lejeu->joueurtab[1].bonus_nombre) {
                        if ((lejeu->joueurtab[1].nb_bomb == 1 || lejeu->joueurtab[1].nb_bomb == 2) && carte[lejeu->joueurtab[1].pos_x][lejeu->joueurtab[1].pos_y] == VIDE) {
                            if(lejeu->joueurtab[1].sac[b%2].posee) b++;
                            posebomb(carte, &lejeu->joueurtab[1], &lejeu->joueurtab[1].sac[b % 2], carte_danger);
                        }
                    }
                }
            }
        }


        //actions des bots.
        for(i=lejeu->nb_joueur; i<lejeu->nb_joueur+lejeu->nb_IA; i++){
            if(!lejeu->joueurtab[i].se_deplace && lejeu->joueurtab[i].vivant){
                IA_poser_bombe(lejeu->joueurtab+i, lejeu, carte_danger, carte, &b);
                mouvement_IA(lejeu->joueurtab+i, carte_danger, carte);
            }
        }


        //Lancement des explosions
        tps = SDL_GetTicks();
        for (i = 0; i < lejeu->nb_IA + lejeu->nb_joueur; i++) {
            if (lejeu->joueurtab[i].bonus_temps) bada = COMPTE_A_REBOURS_INIT - 1000;
            else bada = COMPTE_A_REBOURS_INIT;
            if (lejeu->joueurtab[i].sac[0].posee == TRUE) {
                if ((tps - lejeu->joueurtab[i].sac[0].temps) >= bada)
                {
                    Boom(carte, &lejeu->joueurtab[i].sac[0], &lejeu->joueurtab[i], ecran, boom, lejeu->joueurtab, carte_danger, lejeu->nb_joueur+lejeu->nb_IA, bruit);
                }
            }
            else if (lejeu->joueurtab[i].sac[1].posee == TRUE)
            {
                if ((tps - lejeu->joueurtab[i].sac[1].temps) >= bada)
                {
                    Boom(carte, &lejeu->joueurtab[i].sac[1], &lejeu->joueurtab[i], ecran, boom, lejeu->joueurtab, carte_danger, lejeu->nb_joueur+lejeu->nb_IA, bruit);
                }
            }
        }



        //Ensuite, on affiche la carte
        //Positionnement des coins
        position.x = 0;
        position.y = 0;
        SDL_BlitSurface(coin, &coinclip[HAUT], ecran, &position); //HAUT = en haut à gauche
        position.x = LARGEUR_FENETRE - TAILLE_BLOC;
        position.y = HAUTEUR_FENETRE - TAILLE_BLOC;
        SDL_BlitSurface(coin, &coinclip[BAS], ecran, &position); //BAS = en bas à droite
        position.x = 0;
        position.y = HAUTEUR_FENETRE - TAILLE_BLOC;
        SDL_BlitSurface(coin, &coinclip[GAUCHE], ecran, &position); //GAUCHE = en bas à gauche
        position.x = LARGEUR_FENETRE - TAILLE_BLOC;
        position.y = 0;
        SDL_BlitSurface(coin, &coinclip[DROITE], ecran, &position); //DROITE = en haut à droite.


        //Positionnement des murs.
        for (i = 1; i < NB_BLOC_X - 1; i++) {
            position.x = TAILLE_BLOC*i;
            position.y = 0;
            SDL_BlitSurface(mur, &murclip[HAUT], ecran, &position);
            position.y = TAILLE_BLOC*(NB_BLOC_Y - 1);
            SDL_BlitSurface(mur, &murclip[BAS], ecran, &position);
        }
        for (i = 1; i < NB_BLOC_Y - 1; i++) {
            position.y = TAILLE_BLOC*i;
            position.x = 0;
            SDL_BlitSurface(mur, &murclip[GAUCHE], ecran, &position);
            position.x = TAILLE_BLOC*(NB_BLOC_X - 1);
            SDL_BlitSurface(mur, &murclip[DROITE], ecran, &position);
        }

        //Positionnement du reste de la carte
        for (i = 1; i < NB_BLOC_X - 1; i++) {
            for (j = 1; j < NB_BLOC_Y - 1; j++) {
                position.x = i*TAILLE_BLOC;
                position.y = j*TAILLE_BLOC;
                switch (carte[i][j]) {
                case BLOC: //Blocs indestructibles
                    SDL_BlitSurface(bloc, NULL, ecran, &position);
                    break;
                case ROCHER:
                    SDL_BlitSurface(sol, NULL, ecran, &position);
                    SDL_BlitSurface(rock, NULL, ecran, &position);
                    break;
                case VIDE:
                    SDL_BlitSurface(sol, NULL, ecran, &position);
                    break;
                case BOMBE:
                    SDL_BlitSurface(sol, NULL, ecran, &position);
                    position.x = i*TAILLE_BLOC + (TAILLE_BLOC-BOMBE_TAILLE_X)/2;
                    position.y = j*TAILLE_BLOC + (TAILLE_BLOC-BOMBE_TAILLE_Y)/2;
                    SDL_BlitSurface(bomb, NULL, ecran, &position);
                    break;
                case MBOMB:
                    SDL_BlitSurface(sol, NULL, ecran, &position); //Affichage du bonus
                    position.x = i*TAILLE_BLOC + (TAILLE_BLOC-20)/2;
                    position.y = j*TAILLE_BLOC + (TAILLE_BLOC-25)/2;
                    SDL_BlitSurface(mbomb, NULL, ecran, &position);
                    for (check = 0; check < lejeu->nb_IA + lejeu->nb_joueur; check++) {
                        if (lejeu->joueurtab[check].pos_x == i
                            && lejeu->joueurtab[check].pos_y == j) { //Ramassage du bonus
                            if(Mix_Playing(1)){
                                Mix_HaltChannel(1); //Si un son de bonus jouait déjà, on l'interromp
                            }
                            Mix_PlayChannel(1, sonbonus, 0);
                            if(lejeu->joueurtab[check].bonus_nombre == FALSE){ //Permet de poser deux bombes à la fois.
                                lejeu->joueurtab[check].bonus_nombre = TRUE;
                                lejeu->joueurtab[check].nb_bomb++;
                            }
                            carte[i][j] = VIDE;
                        }
                    }
                    break;
                case RANGE:  //Bonus de portée cumulable
                    SDL_BlitSurface(sol, NULL, ecran, &position); //Affichage du bonus
                    position.x = i*TAILLE_BLOC + (TAILLE_BLOC-23)/2;
                    position.y = j*TAILLE_BLOC + (TAILLE_BLOC-23)/2;
                    SDL_BlitSurface(range, NULL, ecran, &position);
                    for (check = 0; check < lejeu->nb_joueur + lejeu->nb_IA; check++) {
                        if (lejeu->joueurtab[check].pos_x == i
                            && lejeu->joueurtab[check].pos_y == j) { //Ramassage du bonus
                            if(Mix_Playing(1)){
                                Mix_HaltChannel(1); //Si un son de bonus jouait déjà, on l'interromp
                            }
                            Mix_PlayChannel(1, sonbonus, 0);
                            lejeu->joueurtab[check].bonus_portee++; //Augmente la portée des bombes de 1.
                            carte[i][j] = VIDE;
                        }
                    }
                    break;
                case SPEED: //Bonus de vitesse de déplacement non cumulable
                    SDL_BlitSurface(sol, NULL, ecran, &position); //Affichage dy bonus
                    position.x = i*TAILLE_BLOC + (TAILLE_BLOC-24)/2;
                    position.y = j*TAILLE_BLOC + (TAILLE_BLOC-20)/2;
                    SDL_BlitSurface(speed, NULL, ecran, &position);
                    for (check = 0; check < lejeu->nb_joueur + lejeu->nb_IA; check++) {
                        if (lejeu->joueurtab[check].pos_x == i
                            && lejeu->joueurtab[check].pos_y == j) { //Ramassage du bonus
                            if(Mix_Playing(1)){
                                Mix_HaltChannel(1); //Si un son de bonus jouait déjà, on l'interromp
                            }
                            Mix_PlayChannel(1, sonbonus, 0);
                            lejeu->joueurtab[check].vitesse = VITESSE_BONUS; //Augmente la vitesse de déplacement.
                            carte[i][j] = VIDE;
                        }
                    }
                    break;
                case MHEART: //+1 vie, en mode vie.
                    if (lejeu->mode_temps == 0) {
                        SDL_BlitSurface(sol, NULL, ecran, &position); //Affichage du bonus
                        position.x = i*TAILLE_BLOC + (TAILLE_BLOC-20)/2;
                        position.y = j*TAILLE_BLOC + (TAILLE_BLOC-18)/2;
                        SDL_BlitSurface(mheart, NULL, ecran, &position);
                        for (check = 0; check < lejeu->nb_joueur + lejeu->nb_IA; check++) {
                            if (lejeu->joueurtab[check].pos_x == i
                                && lejeu->joueurtab[check].pos_y == j) { //Ramassage du bonus
                                if(Mix_Playing(1)){
                                    Mix_HaltChannel(1); //Si un son de bonus jouait déjà, on l'interromp
                                }
                                Mix_PlayChannel(1, sonbonus, 0);
                                lejeu->joueurtab[check].vie++; //Rend une vie
                                carte[i][j] = VIDE;
                            }
                        }
                    }
                    else carte[i][j] = VIDE;
                    break;
                case TIME: //Bombe qui explose plus rapidement, non cumulable
                    SDL_BlitSurface(sol, NULL, ecran, &position); //Affichage du bonus
                    position.x = i*TAILLE_BLOC + (TAILLE_BLOC-20)/2;
                    position.y = j*TAILLE_BLOC + (TAILLE_BLOC-20)/2;
                    SDL_BlitSurface(drop, NULL, ecran, &position);
                    for (check = 0; check < lejeu->nb_joueur + lejeu->nb_IA; check++) {
                        if (lejeu->joueurtab[check].pos_x == i
                            && lejeu->joueurtab[check].pos_y == j) { //Ramassage du bonus
                            if(Mix_Playing(1)){
                                Mix_HaltChannel(1); //Si un son de bonus jouait déjà, on l'interromp
                            }
                            Mix_PlayChannel(1, sonbonus, 0);
                            lejeu->joueurtab[check].bonus_temps = TRUE; //Les bombes explosent plus vite
                            carte[i][j] = VIDE;
                        }
                    }

                    break;
                }
            }
        }




        survivants = 0;
        for (i = 0; i < lejeu->nb_joueur + lejeu->nb_IA; i++) {
            if (lejeu->mode_temps == 0) survivants += ange_de_la_mort(&lejeu->joueurtab[i]); //verification des  signes de vies des joueurs
            if (lejeu->joueurtab[i].vivant) {
                if (lejeu->joueurtab[i].se_deplace) deplacement(lejeu->joueurtab + i); //déplacement des joueurs
                //et affichage des joueurs
                SDL_BlitSurface(lejeu->joueurtab[i].sprite, &lejeu->joueurtab[i].clip[lejeu->joueurtab[i].sens][lejeu->joueurtab[i].anim_frame], ecran, &lejeu->joueurtab[i].pos_pixel);
            }
        }

        //Tests de la fin de la partie
        tps = SDL_GetTicks();
        SDL_Flip(ecran);
        if (lejeu->mode_temps == 0 && survivants <= 1)
        {
            continuer = 0;
            break;
        }
        else if (lejeu->mode_temps == 1 && tps - tps_debut >= lejeu->duree*MINUTE) {
            continuer = 0;
            break;
        }
    }

    //Libération des surfaces.
	SDL_FreeSurface(mur);
	SDL_FreeSurface(coin);
	SDL_FreeSurface(rock);
	SDL_FreeSurface(sol);
	SDL_FreeSurface(bloc);
	SDL_FreeSurface(bomb);
	SDL_FreeSurface(speed);
	SDL_FreeSurface(mbomb);
	SDL_FreeSurface(mheart);
	SDL_FreeSurface(drop);
	SDL_FreeSurface(range);

	for (i = 0; i < lejeu->nb_joueur + lejeu->nb_IA; i++) {
		SDL_FreeSurface(lejeu->joueurtab[i].sprite);
	}

    //Libération des bruits et des sons.
	Mix_PauseMusic();
	Mix_HaltChannel(-1);
	Mix_FreeChunk(bruit);
	Mix_FreeChunk(sonbonus);
    Mix_FreeMusic(musique);
}

void end_game(SDL_Surface *screen, jeu *game) {
	int *winner, n_win, *comp;
	winner = malloc((game->nb_IA+game->nb_joueur)*sizeof(int));
	comp = malloc((game->nb_IA+game->nb_joueur)*sizeof(int));
	n_win = who_won(game, winner, comp);
	Scores(game, screen, winner, comp, n_win);
	free(winner);
	free(comp);
	return;
}

int who_won(jeu *game, int *winner, int *comp) {
	int i, j, temp, k = 0;
	for (i = 0; i < game->nb_joueur+game->nb_IA; i++) { comp[i] = 0; }
	switch (game->mode_temps)
	{
	case 0:
		for (i = 0; i < game->nb_IA + game->nb_joueur; i++) {
			if (game->joueurtab[i].vivant) winner[0] = game->joueurtab[i].num;
			for (j = 0; j < game->nb_IA + game->nb_joueur; j++) comp[i] += game->joueurtab[i].touche[j];
			comp[i] -= game->joueurtab[i].touche[i];
		}
		return 1;
		break;

	case 1:
		for (i = 0; i < game->nb_IA + game->nb_joueur; i++) {
			for (j = 0; j < game->nb_IA + game->nb_joueur; j++) comp[i] += game->joueurtab[i].touche[j];
			comp[i] -= game->joueurtab[i].touche[i];
			if (i == 0) {
				temp = comp[i];
				winner[k] = game->joueurtab[0].num;
			}
			if (comp[i] > temp) {//recuperation du plus haut score
				temp = comp[i];
				winner[k] = game->joueurtab[i].num;
			}
			else if (comp[i] == temp&&i != 0) {
				k++;
				winner[k] = game->joueurtab[i].num;
			}

		}
		k++;
		return k;
		break;

	default:
		break;
	}
	return k;
}

void initjeu(jeu* lejeu, Choix liste_choix, int map_choisie) {



    //Cette donne des valeurs de départ et alloue de la mémoire pour le jeu, ses joueurs et leurs bombes
	int i, j, k;
	lejeu->nb_joueur = liste_choix.nb_joueur;
	lejeu->nb_IA = liste_choix.nb_ia;
	lejeu->mode_temps = liste_choix.mode;
	lejeu->niveau = map_choisie;
	lejeu->again = TRUE;
	lejeu->duree = liste_choix.duree; //pour l'instant on s'en fiche.
	lejeu->joueurtab = malloc((lejeu->nb_joueur + lejeu->nb_IA)*sizeof(joueur));
	for (i = 0; i <= lejeu->nb_joueur + lejeu->nb_IA - 1; i++) { //On parcourt la liste des joueurs
		lejeu->joueurtab[i].num = i + 1;
		if (i <= lejeu->nb_joueur - 1) lejeu->joueurtab[i].est_IA = FALSE;
		else lejeu->joueurtab[i].est_IA = TRUE;
		lejeu->joueurtab[i].vie = liste_choix.vies;
		lejeu->joueurtab[i].se_deplace = FALSE;
		lejeu->joueurtab[i].vitesse = VITESSE_INITIALE;
		lejeu->joueurtab[i].bonus_nombre = FALSE;
		lejeu->joueurtab[i].bonus_portee = 0;
		lejeu->joueurtab[i].bonus_temps = FALSE;
		lejeu->joueurtab[i].anim_frame = 0;
		lejeu->joueurtab[i].anim_chrono = 0;
		lejeu->joueurtab[i].nb_bomb = 1;
		lejeu->joueurtab[i].delai_bombe= 0;
		lejeu->joueurtab[i].vivant = TRUE;
		for (k = 0; k < 2; k++) {
			lejeu->joueurtab[i].sac[k].num_joueur = lejeu->joueurtab[i].num;
			lejeu->joueurtab[i].sac[k].posee = FALSE;
		}
		for (j = 0; j < 4; j++) { //Pour chaque sens de déplacement
			for (k = 0; k < 10; k++) { //Pour chaque frame de l'animation
				lejeu->joueurtab[i].clip[j][k].x = SPRITE_X*j;
				lejeu->joueurtab[i].clip[j][k].y = SPRITE_Y*k;
				lejeu->joueurtab[i].clip[j][k].w = SPRITE_X;
				lejeu->joueurtab[i].clip[j][k].h = SPRITE_Y;
			}
		}
	}

	//Placement initial du joueur 1.
	lejeu->joueurtab[0].sprite = SDL_LoadBMP("Image/Dr Fetus.bmp");
	SDL_SetColorKey(lejeu->joueurtab[0].sprite, SDL_SRCCOLORKEY, SDL_MapRGB(lejeu->joueurtab[0].sprite->format, 0, 255, 0));
	switch (map_choisie)
	{
	case 1:
		lejeu->joueurtab[0].pos_x = 1;
		lejeu->joueurtab[0].pos_y = 1;
		break;
	case 2:
		lejeu->joueurtab[0].pos_x = 1;
		lejeu->joueurtab[0].pos_y = 1;
		break;
	case 3:
		lejeu->joueurtab[0].pos_x = 2;
		lejeu->joueurtab[0].pos_y = 2;
		break;
	case 4:
		lejeu->joueurtab[0].pos_x = 2;
		lejeu->joueurtab[0].pos_y = 2;
		break;
	case 5:
		lejeu->joueurtab[0].pos_x = 6;
		lejeu->joueurtab[0].pos_y = 4;
		break;
	case 6:
		lejeu->joueurtab[0].pos_x = 1;
		lejeu->joueurtab[0].pos_y = 1;
		break;
	default:
		break;
	}
	lejeu->joueurtab[0].pos_pixel.x = lejeu->joueurtab[0].pos_x *TAILLE_BLOC + (TAILLE_BLOC - SPRITE_X) / 2;
	lejeu->joueurtab[0].pos_pixel.y = lejeu->joueurtab[0].pos_y *TAILLE_BLOC + (TAILLE_BLOC - SPRITE_Y) / 2;
	lejeu->joueurtab[0].sens = BAS;
	for (i = 0; i < 4; i++) {
		lejeu->joueurtab[0].touche[i] = 0;
	}

	//Placement initial du joueur 2.
	if (lejeu->nb_joueur + lejeu->nb_IA >= 2) {
		lejeu->joueurtab[1].sprite = SDL_LoadBMP("Image/Blue Baby.bmp");
		SDL_SetColorKey(lejeu->joueurtab[1].sprite, SDL_SRCCOLORKEY, SDL_MapRGB(lejeu->joueurtab[1].sprite->format, 0, 255, 0));
		switch (map_choisie)
		{
		case 1:
			lejeu->joueurtab[1].pos_x = NB_BLOC_X - 2;
			lejeu->joueurtab[1].pos_y = NB_BLOC_Y - 2;
			break;
		case 2:
			lejeu->joueurtab[1].pos_x = NB_BLOC_X - 2;
			lejeu->joueurtab[1].pos_y = NB_BLOC_Y - 2;
			break;
		case 3:
			lejeu->joueurtab[1].pos_x = NB_BLOC_X - 3;
			lejeu->joueurtab[1].pos_y = NB_BLOC_Y - 3;
			break;
		case 4:
			lejeu->joueurtab[1].pos_x = NB_BLOC_X - 3;
			lejeu->joueurtab[1].pos_y = NB_BLOC_Y - 3;
			break;
		case 5:
			lejeu->joueurtab[1].pos_x = 9;
			lejeu->joueurtab[1].pos_y = 7;
			break;
		case 6:
			lejeu->joueurtab[1].pos_x = NB_BLOC_X - 2;
			lejeu->joueurtab[1].pos_y = NB_BLOC_Y - 2;
			break;
		default:
			break;
		}
		lejeu->joueurtab[1].pos_pixel.x = lejeu->joueurtab[1].pos_x *TAILLE_BLOC + (TAILLE_BLOC - SPRITE_X) / 2;
		lejeu->joueurtab[1].pos_pixel.y = lejeu->joueurtab[1].pos_y *TAILLE_BLOC + (TAILLE_BLOC - SPRITE_Y) / 2;
		lejeu->joueurtab[1].sens = GAUCHE;
		for (i = 0; i < 4; i++) {
			lejeu->joueurtab[1].touche[i] = 0;
		}
	}
	//Placement initial du joueur 3.
	if (lejeu->nb_joueur + lejeu->nb_IA >= 3) {
		lejeu->joueurtab[2].sprite = SDL_LoadBMP("Image/Isaac.bmp");
		SDL_SetColorKey(lejeu->joueurtab[2].sprite, SDL_SRCCOLORKEY, SDL_MapRGB(lejeu->joueurtab[2].sprite->format, 0, 255, 0));
		switch (map_choisie)
		{
		case 1:
			lejeu->joueurtab[2].pos_x = 1;
			lejeu->joueurtab[2].pos_y = NB_BLOC_Y - 2;
			break;
		case 2:
			lejeu->joueurtab[2].pos_x = 1;
			lejeu->joueurtab[2].pos_y = NB_BLOC_Y - 2;
			break;
		case 3:
			lejeu->joueurtab[2].pos_x = 2;
			lejeu->joueurtab[2].pos_y = NB_BLOC_Y - 3;
			break;
		case 4:
			lejeu->joueurtab[2].pos_x = 1;
			lejeu->joueurtab[2].pos_y = NB_BLOC_Y - 2;
			break;
		case 5:
			lejeu->joueurtab[2].pos_x = 6;
			lejeu->joueurtab[2].pos_y = 7;
			break;
		case 6:
			lejeu->joueurtab[2].pos_x = 1;
			lejeu->joueurtab[2].pos_y = NB_BLOC_Y - 2;
			break;
		default:
			break;
		}
		lejeu->joueurtab[2].pos_pixel.x = lejeu->joueurtab[2].pos_x *TAILLE_BLOC + (TAILLE_BLOC - SPRITE_X) / 2;
		lejeu->joueurtab[2].pos_pixel.y = lejeu->joueurtab[2].pos_y *TAILLE_BLOC + (TAILLE_BLOC - SPRITE_Y) / 2;
		lejeu->joueurtab[2].sens = DROITE;
		for (i = 0; i < 4; i++) {
			lejeu->joueurtab[2].touche[i] = 0;
		}
	}
	//Placement initial du joueur 4.
	if (lejeu->nb_joueur + lejeu->nb_IA == 4) {
		lejeu->joueurtab[3].sprite = SDL_LoadBMP("Image/Bob.bmp");
		SDL_SetColorKey(lejeu->joueurtab[3].sprite, SDL_SRCCOLORKEY, SDL_MapRGB(lejeu->joueurtab[3].sprite->format, 0, 255, 0));
		switch (map_choisie)
		{
		case 1:
			lejeu->joueurtab[3].pos_x = NB_BLOC_X - 2;
			lejeu->joueurtab[3].pos_y = 1;
			break;
		case 2:
			lejeu->joueurtab[3].pos_x = NB_BLOC_X - 2;
			lejeu->joueurtab[3].pos_y = 1;
			break;
		case 3:
			lejeu->joueurtab[3].pos_x = NB_BLOC_X - 3;
			lejeu->joueurtab[3].pos_y = 2;
			break;
		case 4:
			lejeu->joueurtab[3].pos_x = NB_BLOC_X - 2;
			lejeu->joueurtab[3].pos_y = 1;
			break;
		case 5:
			lejeu->joueurtab[3].pos_x = 9;
			lejeu->joueurtab[3].pos_y = 4;
			break;
		case 6:
			lejeu->joueurtab[3].pos_x = NB_BLOC_X - 2;
			lejeu->joueurtab[3].pos_y = 1;
			break;
		default:
			break;
		}
		lejeu->joueurtab[3].pos_pixel.x = lejeu->joueurtab[3].pos_x *TAILLE_BLOC + (TAILLE_BLOC - SPRITE_X) / 2;
		lejeu->joueurtab[3].pos_pixel.y = lejeu->joueurtab[3].pos_y *TAILLE_BLOC + (TAILLE_BLOC - SPRITE_Y) / 2;
		lejeu->joueurtab[3].sens = BAS;
		for (i = 0; i < 4; i++) {
			lejeu->joueurtab[3].touche[i] = 0;
		}
	}


	return;
}

void commence_deplacement(joueur* lejoueur, int** carte, int direction) {

	//Cette fonction ne déplace pas le joueur, mais quand le joueur demande à se déplacer via le clavier, il vérifie si cette entrée sera prise en compte.
	if (!lejoueur->se_deplace) {


		switch (direction) {
			//Pour chaque cas, on commence par tester que la case où on va est libre
		case HAUT:
			if (carte[lejoueur->pos_x][lejoueur->pos_y - 1] == VIDE || carte[lejoueur->pos_x][lejoueur->pos_y - 1] >= MBOMB) {
				lejoueur->sens = HAUT;
				lejoueur->se_deplace = TRUE;
				lejoueur->anim_chrono = SDL_GetTicks();
				break;
			}
			break;

		case BAS:
			if (carte[lejoueur->pos_x][lejoueur->pos_y + 1] == VIDE || carte[lejoueur->pos_x][lejoueur->pos_y + 1] >= MBOMB) {
				lejoueur->sens = BAS;
				lejoueur->se_deplace = TRUE;
				lejoueur->anim_chrono = SDL_GetTicks();
				break;
			}
			break;

		case GAUCHE:
			if (carte[lejoueur->pos_x - 1][lejoueur->pos_y] == VIDE || carte[lejoueur->pos_x - 1][lejoueur->pos_y] >= MBOMB) {
				lejoueur->sens = GAUCHE;
				lejoueur->se_deplace = TRUE;
				lejoueur->anim_chrono = SDL_GetTicks();
			}
			break;

		case DROITE:
			if (carte[lejoueur->pos_x + 1][lejoueur->pos_y] == VIDE || carte[lejoueur->pos_x + 1][lejoueur->pos_y] >= MBOMB) {
				lejoueur->sens = DROITE;
				lejoueur->se_deplace = TRUE;
				lejoueur->anim_chrono = SDL_GetTicks();
			}
			break;
		}

	}
}

void deplacement(joueur* lejoueur) {


    //Cette joue l'animation et déplace petit à petit le joueur vers la case suivante.
	int temps_ecoule = SDL_GetTicks() - lejoueur->anim_chrono; //temps écoulé depuis le début de l'animation.
	lejoueur->anim_frame = (temps_ecoule)* 10 / lejoueur->vitesse;
	//On multiplie par 10 car il y a 10 frames d'animation.

	switch (lejoueur->sens) {

	case HAUT:
		//La position du joueur centré sur son ancienne case
		//plus ou moins un nombre de pixels dépendant du temps écoulé depuis le début de l'animation.

		if (temps_ecoule > lejoueur->vitesse) {
			lejoueur->pos_y--;
			lejoueur->se_deplace = FALSE;
			lejoueur->anim_frame = 0;
			lejoueur->pos_pixel.y = lejoueur->pos_y*TAILLE_BLOC + (TAILLE_BLOC - SPRITE_Y) / 2;
		}
		else {
			lejoueur->pos_pixel.y = lejoueur->pos_y*TAILLE_BLOC + (TAILLE_BLOC - SPRITE_Y) / 2 - (temps_ecoule)*TAILLE_BLOC / lejoueur->vitesse;
		}
		break;


	case BAS:
		if (temps_ecoule > lejoueur->vitesse) {
			lejoueur->pos_y++;
			lejoueur->se_deplace = FALSE;
			lejoueur->anim_frame = 0;
			lejoueur->pos_pixel.y = lejoueur->pos_y*TAILLE_BLOC + (TAILLE_BLOC - SPRITE_Y) / 2;
		}
		else {
			lejoueur->pos_pixel.y = lejoueur->pos_y*TAILLE_BLOC + (TAILLE_BLOC - SPRITE_Y) / 2 + (temps_ecoule)*TAILLE_BLOC / lejoueur->vitesse;
		}
		break;

	case GAUCHE:
		if (temps_ecoule > lejoueur->vitesse) {
			lejoueur->pos_x--;
			lejoueur->se_deplace = FALSE;
			lejoueur->anim_frame = 0;
			lejoueur->pos_pixel.x = lejoueur->pos_x*TAILLE_BLOC + (TAILLE_BLOC - SPRITE_X) / 2;
		}
		else {
			lejoueur->pos_pixel.x = lejoueur->pos_x*TAILLE_BLOC + (TAILLE_BLOC - SPRITE_X) / 2 - (temps_ecoule)*TAILLE_BLOC / lejoueur->vitesse;
		}
		break;

	case DROITE:

		if (temps_ecoule > lejoueur->vitesse) {
			lejoueur->pos_x++;
			lejoueur->se_deplace = FALSE;
			lejoueur->anim_frame = 0;
			lejoueur->pos_pixel.x = lejoueur->pos_x*TAILLE_BLOC + (TAILLE_BLOC - SPRITE_X) / 2;
		}
		else {
			lejoueur->pos_pixel.x = lejoueur->pos_x*TAILLE_BLOC + (TAILLE_BLOC - SPRITE_X) / 2 + (temps_ecoule)*TAILLE_BLOC / lejoueur->vitesse;
		}
		break;

	}

}




void mouvement_IA(joueur* IA, int carte_danger[][NB_BLOC_Y], int** carte){

    //décide du déplacement de l'IA


    //L'IA est en danger : choix defensif. On appelle un pathfinder qui va cherche le chemin le plus court pour que l'IA soit en sécurité.
    //Note : le posage de bombe a pu le mettre en danger.
    if(carte_danger[IA->pos_x][IA->pos_y]!=0){
        int i, j;
        int choix;
        int tab_chemin[NB_BLOC_X][NB_BLOC_Y];
        //On copie la carte normale des collisions.
        for(i=0; i<NB_BLOC_X; i++){
            for(j=0; j<NB_BLOC_Y; j++){
                tab_chemin[i][j] = carte[i][j];
            }
        }
        tab_chemin[IA->pos_x][IA->pos_y] = -1;
        int* min = malloc(sizeof(int));
        *min = 10;

        choix = IA_pathfinding(IA->pos_x, IA->pos_y, 1, min, carte_danger, tab_chemin);
        if(choix != -1) commence_deplacement(IA, carte, choix);


    }


    /*L'IA est safe, on se déplace au hasard.
    Elle ne doit viser ni une case bloquée par un obstacle, ni une zone de danger.
    On édite le tableau des directions possibles en enlevant les directions où on trouve un obstacle ou une zone dangereuse.
    Puis on tire dans ce tableau une direction aléatoire parmi celles restantes.
    */
    else{
        int temp, r, i, nb_possibilite = 4;
        int directions_possibles[4] = {BAS, HAUT, DROITE, GAUCHE};

        if(carte_danger[IA->pos_x+1][IA->pos_y] != 0 || !(carte[IA->pos_x+1][IA->pos_y] == VIDE || carte[IA->pos_x+1][IA->pos_y] >= MBOMB)){
            //Si la case du dessus est dangereuse ou qu'il y a un obstacle, on inverse le côté "droit" avec la dernière case du tableau des directions.
            for(i=0; i<nb_possibilite-1; i++){
                if(directions_possibles[i] == DROITE){
                    temp = directions_possibles[i];
                    directions_possibles[i]=directions_possibles[nb_possibilite-1];
                    directions_possibles[nb_possibilite-1] = temp;
                }
            }
            nb_possibilite--;
        }
        if(carte_danger[IA->pos_x-1][IA->pos_y] != 0 || !(carte[IA->pos_x-1][IA->pos_y] == VIDE || carte[IA->pos_x-1][IA->pos_y] >= MBOMB)){
            //idem pour le case "gauche".
            for(i=0; i<nb_possibilite-1; i++){
                if(directions_possibles[i] == GAUCHE){
                    temp = directions_possibles[i];
                    directions_possibles[i]=directions_possibles[nb_possibilite-1];
                    directions_possibles[nb_possibilite-1] = temp;
                }
            }
            nb_possibilite--;
        }
        if(carte_danger[IA->pos_x][IA->pos_y+1] != 0 || !(carte[IA->pos_x][IA->pos_y+1]== VIDE || carte[IA->pos_x][IA->pos_y+1] >= MBOMB)){
            //idem pour le bas.
            for(i=0; i<nb_possibilite-1; i++){
                if(directions_possibles[i] == BAS){
                    temp = directions_possibles[i];
                    directions_possibles[i]=directions_possibles[nb_possibilite-1];
                    directions_possibles[nb_possibilite-1] = temp;
                }
            }
            nb_possibilite--;
        }
        if(carte_danger[IA->pos_x][IA->pos_y-1] != 0 || !(carte[IA->pos_x][IA->pos_y-1] == VIDE || carte[IA->pos_x][IA->pos_y-1] >= MBOMB)){
            //idem pour le haut.
            for(i=0; i<nb_possibilite-1; i++){
                if(directions_possibles[i] == HAUT){
                    temp = directions_possibles[i];
                    directions_possibles[i]=directions_possibles[nb_possibilite-1];
                    directions_possibles[nb_possibilite-1] = temp;
                }
            }
            nb_possibilite--;
        }
        //On choisit le déplacement du bot aléatoirement dans le tableau des directions possibles.
        if(nb_possibilite>0){
            r=rand()%nb_possibilite;
            commence_deplacement(IA, carte, directions_possibles[r]);

        }
    }
}

void IA_poser_bombe(joueur* IA, jeu* lejeu, int carte_danger[][NB_BLOC_Y], int** carte, int* b){

    int i,j,  tps, r;
    int tab_chemin[NB_BLOC_X][NB_BLOC_Y];

    //Si le joueur est en sécurité, il a une chance de poser une bombe. Plus il va toucher de cailloux, plus les chances sont élevées.
    int cailloux = 0; //ce booléen compte le nombre de cailloux à portée du joueur

    Bombe simule; //On simule le posage d'une bombe fictive
    simule.range = IA->bonus_portee+1;
    simule.posx = IA->pos_x;
    simule.posy = IA->pos_y;
    simule.num_joueur = 10;
    ajouter_danger_carte(simule, carte, carte_danger); //On édite la carte des dangers avec cette bombe

    for(i=0; i<NB_BLOC_X; i++){  //On crée notre table des chemins possibles
        for(j=0; j<NB_BLOC_Y; j++){
            tab_chemin[i][j] = carte[i][j];
        }
    }
    tab_chemin[IA->pos_x][IA->pos_y] = -1;
    int* min = malloc(sizeof(int));
    *min = 10;
     //Si aucun chemin possible n'est détecté, on quitte la fonction sans poser de bombe.
    if(IA_pathfinding(IA->pos_x, IA->pos_y, 1, min, carte_danger, tab_chemin) == -1){
        enlever_danger_carte(simule, carte, carte_danger); //On enlève cette bombe fictive du tableau des dangers.
        free(min);
        return;
    }
    enlever_danger_carte(simule, carte, carte_danger);
    free(min);


    //Si l'IA peut poser cette bombe sans être sure de mourir, on continue.

    //L'IA compte le nonmbre de cailloux qu'elle peut détruire en posant un bombe
    //Plus ce nombre est élevé, plus elle a une chance élevée de poser la bombe.
    i=0;
    do{
        i++;
        if(carte[IA->pos_x-i][IA->pos_y] == ROCHER) cailloux++;    //On regarde si poser une bombe à cet endroit lui permettrait de casser un cailloux au dessus de lui.

    }while(i<IA->bonus_portee && carte[IA->pos_x-i][IA->pos_y] != MUR && carte[IA->pos_x-i][IA->pos_y] != BLOC && carte[IA->pos_x-i][IA->pos_y] != ROCHER);

    i=0;
    do{
        i++;
        if(carte[IA->pos_x+i][IA->pos_y] == ROCHER) cailloux++;    //On regarde si poser une bombe à cet endroit lui permettrait de casser un cailloux en dessous de lui.

    }while(i<IA->bonus_portee && carte[IA->pos_x+i][IA->pos_y] != MUR && carte[IA->pos_x+i][IA->pos_y] != BLOC && carte[IA->pos_x+i][IA->pos_y] != ROCHER);

    i=0;
    do{
        i++;
        if(carte[IA->pos_x][IA->pos_y-i] == ROCHER) cailloux++;    //On regarde si poser une bombe à cet endroit lui permettrait de casser un cailloux a gauche de lui.

    }while(i<IA->bonus_portee && carte[IA->pos_x][IA->pos_y-i] != MUR && carte[IA->pos_x][IA->pos_y-i] != BLOC && carte[IA->pos_x][IA->pos_y-i] != ROCHER);

    i=0;
    do{
        i++;
        if(carte[IA->pos_x][IA->pos_y+i] == ROCHER) cailloux++;    //On regarde si poser une bombe à cet endroit lui permettrait de casser un cailloux a droite de lui.

    }while(i<IA->bonus_portee && carte[IA->pos_x][IA->pos_y+i] != MUR && carte[IA->pos_x][IA->pos_y+i] != BLOC && carte[IA->pos_x][IA->pos_y+i] != ROCHER);



    r = rand()%100; //Plus il peut toucher des cailloux, plus il a de chance de poser une bombe.
    if(r<=15*(cailloux+i)){
        tps = SDL_GetTicks();
        if(tps-IA->delai_bombe > DELAI_BOMBE){
            if (!IA->bonus_nombre && IA->nb_bomb == 1)	posebomb(carte, IA, &IA->sac[0], carte_danger);
            else if (IA->bonus_nombre) {
                if ((IA->nb_bomb == 1 || IA->nb_bomb == 2) && carte[IA->pos_x][IA->pos_y] == VIDE) {
                    if(IA->sac[*b%2].posee) *b=*b+1;
                    posebomb(carte, IA, &IA->sac[*b % 2], carte_danger);
                }
            }
        }
    }
}




int IA_pathfinding(int x, int y, int distance, int* min, int carte_danger[][NB_BLOC_Y], int tab_chemin[][NB_BLOC_Y]){

    //Fonction récursive du démon, qui cherche la zone de sécurité la plus proche.

    if(distance > 8) return -1; //On cherche un chemin pour s'échapper avec une distance maximale de 8.
    int choix = -1;

    if(tab_chemin[x+1][y]==VIDE || tab_chemin[x+1][y] >= MBOMB || (tab_chemin[x+1][y]<0 && tab_chemin[x+1][y]<tab_chemin[x][y])){
        tab_chemin[x+1][y] = -distance-1;
        if(carte_danger[x+1][y] == 0){
            if(*min > -tab_chemin[x+1][y]){ //Dès qu'on trouve une zone safe, si elle est à une distance inférieure à la minimale trouvée, on dit que c'est la minimale.
                *min = distance+1; //On stocke dans un tableau le décors, et les distances au point d'origine avec un moins devant pour ne pas confondre avec le décors.
                choix = DROITE;
            }
        }
        else{
            if(IA_pathfinding(x+1, y, distance+1, min, carte_danger, tab_chemin)!=-1) //Si la zone regardée n'est pas safe, on regarde ses zones voisines avec une distance augmentée de 1.
                choix = DROITE;
        }
    }

    //idem pour les cas gauche, haut et bas.
    if(tab_chemin[x-1][y]==VIDE || tab_chemin[x-1][y] >= MBOMB || (tab_chemin[x-1][y]<0 && tab_chemin[x-1][y]<tab_chemin[x][y])){
        tab_chemin[x-1][y] = -distance-1;
        if(carte_danger[x-1][y] == 0){
            if(*min > -tab_chemin[x-1][y]){
                *min = distance+1;
                choix = GAUCHE;
            }
        }
        else{
            if(IA_pathfinding(x-1, y, distance+1, min, carte_danger, tab_chemin)!=-1)
                choix = GAUCHE;
        }
    }
    if(tab_chemin[x][y-1]==VIDE || tab_chemin[x][y-1] >= MBOMB || (tab_chemin[x][y-1]<0 && tab_chemin[x][y-1]<tab_chemin[x][y])){
        tab_chemin[x][y-1] = -distance-1;
        if(carte_danger[x][y-1] == 0){
            if(*min > -tab_chemin[x][y-1]){
                *min = distance+1;
                choix = HAUT;
            }
        }
        else{
            if(IA_pathfinding(x, y-1, distance+1, min, carte_danger, tab_chemin)!=-1)
                choix = HAUT;
        }
    }
    if(tab_chemin[x][y+1]==VIDE || tab_chemin[x][y+1] >= MBOMB || (tab_chemin[x][y+1]<0 && tab_chemin[x][y+1]<tab_chemin[x][y])){
        tab_chemin[x][y+1] = -distance-1;
        if(carte_danger[x][y+1] == 0){
            if(*min > -tab_chemin[x][y+1]){
                *min = distance+1;
                choix = BAS;
            }
        }
        else{
            if(IA_pathfinding(x, y+1, distance+1, min, carte_danger, tab_chemin)!=-1)
                choix = BAS;
        }
    }

    return choix;
     //Si choix == -1, on n'a pas trouvé de zone safe avec un plus petite distance que min.
     //Sinon, on renvoie la direction qu'on a pris pour aller à l'endroit trouvé.

}
