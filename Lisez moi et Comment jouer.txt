Ceci est un fangame Bomberman dans l'univers de The Binding of Isaac, créé par Pierre-Antoine Locatelli et Bryan Vigee
C'était un projet universitaire fait pendant nos études à l'école d'ingénieur Polytech Paris-Sud, en 2016. Il a été codé en langage C, avec la bibliothèque SDL.

Comment jouer :
Lancez le fichier "Cleromancy.exe" pour commencer à jouer.
Naviguez dans le menu en utilisant les flèches directionnelles, la touche "Entrée" pour valider vos choix, et la touche "Retour Arrière" pour revenir au choix précédent.
Commencez par choisir de jouer seul, ou avec un ami sur le même ordinateur.
Ensuite, choisissez le nombre d'ennemis contrôlé par l'ordinateur et/ou par des joueurs.
Choisissez ensuite une condition de fin :
- Si vous choisissez le "temps", le jeu s'arrêter automatiquement après le nombre spécifié de minutes. Le jouer qui aura touché le plus d'ennemis avec ses bombes aura gagné.
- Si vous choisissez les "vies", les personnages mourront après avoir été touché par des explosions un nombre spécifié de fois. Le jeu se termine quand il n'y a plus qu'un personnage en vie.
Enfin, choisissez une des cartes disponibles pour lancer la partie


Contrôles :
- Joueur 1 : z pour monter, s pour descendre, q pour aller à gauche, d pour aller à droite et maj gauche pour poser une bombe.
- Joueur 2 : les flèches pour bouger et maj droite pour poser une bombe.
Vous pouvez terminer le jeu manuellement en appuyant sur la touche "Échap".

Bonus : 
- Horloge : réduit le délai avant explosion de vos bombes
- Bombes : vous pouvez avoir plusieurs bombes posées simultanément
- Rollers : votre personnage se déplace plus vite
- Explosion : vos explosions gagnent en portée
- Heart : votre personnage gagne en portée


Ce jeu utilise des images et des musiques tirées du jeu vidéo The Binding of Isaac. Tout les droits de propriété sur ces élements appartiennent à leurs propriétaires respectifs. The Binding of Isaac est un jeu créé par Edmund McMillen.
Si vous êtes propriétaires de ces élément et que vous voulez faire retirer le jeu, n'hésitez pas à me contacter par mail à l'adresse locatelli.pierreantoine@gmail.com avant d'entamer tout procédure juridique.
