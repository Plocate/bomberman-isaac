This is a Bomberman fanmade game in the univers of The Binding of Isaac, created by Pierre-Antoine Locatelli and Bryan Vigee
It was a college project made during our studies at the school of engineering Polytech Paris-Sud, in 2016. It was coded in C language and using SDL library.


How to play:
Start the game by launching the file named "Cleromancy.exe"
You can navigate the menu using arrows and "Enter" key to valide your choice, or "Return" key to return to previous choice.
Start by choosing if you want to play alone or with a friend on the same computer.
Then chose the number of computer ennemies and/or players that will play together.
Then chose an ending condition :
- If you picked "time", chose the game will end automatically after the specified number of minutes. The player who hit the most number of ennemies with their bombs wins the game.
- If you picked "lives", a character will die when he is hit by a bomb the specified number of times. The game ends when there is only one character remaining.
Finally, pick a map to start the game.

Controls:
- Player 1: z to go up, s to go down, q to go left, d to go right, and left shift to drop a bomb.
- Player 2: arrows to move, right shift to drop a bomb.
You can end the game early by pressing "Esc".

Bonuses : 
- Clock : reduce delay before the bombs explode
- Bomb : you may have several bombs on field at the same time
- Rollers : your character moves faster
- Explosion : your explosions gain range
- Heart : your character regains one life


This game plays musics and displays graphics from the video game The Binding of Isaac.
All rights on those assets belong to their respective owners. The Binding of Isaac is a game created by Edmund McMillen.
If you own those assets and want us to remove this game from the internet, please feel free to contact us at locatelli.pierreantoine@gmail.com before initiating any legal actions.
