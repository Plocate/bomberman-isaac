#include <stdlib.h>
#include <SDL/SDL.h>
#include <time.h>
#include <SDL_mixer.h>
#include "Struct.h"
#include "menu.h"


void Lancer_Menu(SDL_Surface *screen, Choix *set, int *map) {
    Mix_Music *musique;
    Mix_Chunk *bruit;
	SDL_Surface *boutons[5], *sousmenu[10]; //chargement et creation de l'icone
	Mix_AllocateChannels(1);
	musique = Mix_LoadMUS("Son/Infanticide.wav");
	bruit = Mix_LoadWAV("Son/Big Explosion.wav");
	Mix_VolumeMusic(MIX_MAX_VOLUME/8);
	Mix_VolumeChunk(bruit, MIX_MAX_VOLUME/6);
	Mix_PlayChannel(0, bruit, -1);
    Mix_PlayMusic(musique, -1);
	Init_Menu(screen, boutons, sousmenu, set, map);
	int i;
	for (i = 0; i < 5; i++) {
		SDL_FreeSurface(boutons[i]);
	}
	Mix_PauseMusic();
    Mix_FreeMusic(musique);
    Mix_FreeChunk(bruit);
	Mix_HaltChannel(-1);
}

void Init_Menu(SDL_Surface* screen, SDL_Surface **boutons, SDL_Surface **sousmenu, Choix *set, int *map) {
	//4=logo, 3=fond, 0=oneplaybut, 1=pvp_but, 2=quit_but;
	SDL_Rect positionFond, positionLogo, button, position1, position2, position3;

	//chargement des images
	boutons[4] = SDL_LoadBMP("Image/Logo.bmp");
	boutons[3] = SDL_LoadBMP("Image/fond.bmp");
	boutons[0] = SDL_LoadBMP("Image/1p_button.bmp");
	boutons[1] = SDL_LoadBMP("Image/pvp_button.bmp");
	boutons[2] = SDL_LoadBMP("Image/quit_button.bmp");

	//definition des rect de destination et source
	positionFond.x = 0;
	positionFond.y = 0;
	positionLogo.x = (screen->w - boutons[4]->w) / 2;
	positionLogo.y = 30;
	button.w = boutons[0]->w;
	button.h = boutons[0]->h / 2;
	position1.x = (screen->w - boutons[0]->w) / 2;
	position2 = position1;
	position3 = position1;
	position1.y = (boutons[4]->h + 60);
	button.y = 0;
	button.x = 0;

	//positionnement du fond et du titre
	SDL_SetColorKey(boutons[4], SDL_SRCCOLORKEY, SDL_MapRGB(boutons[4]->format, 0, 255, 0));
	SDL_BlitSurface(boutons[3], NULL, screen, &positionFond);
	SDL_BlitSurface(boutons[4], NULL, screen, &positionLogo);
	SDL_Flip(screen);

	//apparition des boutons de selection
	Fondu_apparition(&button, screen, boutons[0], &position1);
	position2.y = position1.y + button.h + 30;
	Fondu_apparition(&button, screen, boutons[1], &position2);
	position3.y = position2.y + button.h + 30;
	Fondu_apparition(&button, screen, boutons[2], &position3);

	//appel des evenements
	Event_Menu(screen, &button, &position1, &position2, &position3, boutons, sousmenu, set, map);
	return;
}

void Fondu_apparition(SDL_Rect *but, SDL_Surface* screen, SDL_Surface* bouton, SDL_Rect *destination) {
	int i;
	if (but) {
		for (i = 0; i <= 255; i++)
		{
			SDL_SetAlpha(bouton, SDL_SRCALPHA, i);
			SDL_BlitSurface(bouton, but, screen, destination);
			SDL_Flip(screen);
		}
	}
	else {
		for (i = 0; i <= 255; i++) {
			SDL_SetAlpha(bouton, SDL_SRCALPHA, i);
			SDL_BlitSurface(bouton, NULL, screen, destination);
			SDL_Flip(screen);
		}
	}SDL_Flip(screen);
	return;
}

void Fondu_disparition(SDL_Surface *fond, SDL_Surface *screen) {
	SDL_Rect pfond;
	pfond.x = 0;
	pfond.y = 0;
	Fondu_apparition(NULL, screen, fond, &pfond);
	return;
}

void Event_Menu(SDL_Surface *screen, SDL_Rect *but, SDL_Rect *destination1, SDL_Rect *destination2, SDL_Rect *destination3, SDL_Surface **boutons, SDL_Surface **sousmenu, Choix *set, int *map) {
	int cont = 1, bouton_vert = 1, y_vert = but->h + 1, y_jaune = 0, retourmenu, i;

	//creation de la variable event
	SDL_Event event;

	but->y = y_vert;//passage au vert
	SDL_BlitSurface(boutons[0], but, screen, destination1);
	SDL_Flip(screen);

	while (cont) {

		while (SDL_PollEvent(&event)) {
			switch (event.type)
			{
			case SDL_QUIT://fermeture de la fenetre
				set->quit = TRUE;
				//SDL_FreeSurface(boutons);
				//SDL_FreeSurface(sousmenu);
				cont = 0;
				return;
				break;

			case SDL_KEYDOWN:
				switch (event.key.keysym.sym)
				{
				case SDLK_DOWN:
					switch (bouton_vert)
					{
					case 1:
						but->y = y_vert;//passage au vert
						SDL_BlitSurface(boutons[1], but, screen, destination2);
						but->y = y_jaune;//remise au jaune
						SDL_BlitSurface(boutons[0], but, screen, destination1);
						SDL_Flip(screen);
						bouton_vert++;
						break;
					case 2:
						but->y = y_vert;//passage au vert
						SDL_BlitSurface(boutons[2], but, screen, destination3);
						but->y = y_jaune;//remise au jaune
						SDL_BlitSurface(boutons[1], but, screen, destination2);
						SDL_Flip(screen);
						bouton_vert++;
						break;
					case 3://on sort de la boucle pour fermer la fenetre
						break;
					}
					break;
				case SDLK_UP:
					switch (bouton_vert)
					{
					case 1:
						break;

					case 2:
						but->y = y_vert;//passage au vert
						SDL_BlitSurface(boutons[0], but, screen, destination1);
						but->y = y_jaune;//remise au jaune
						SDL_BlitSurface(boutons[1], but, screen, destination2);
						SDL_Flip(screen);
						bouton_vert = 1;
						break;
					case 3:
						but->y = y_vert;//passage au vert
						SDL_BlitSurface(boutons[1], but, screen, destination2);
						but->y = y_jaune;//remise au jaune
						SDL_BlitSurface(boutons[2], but, screen, destination3);
						SDL_Flip(screen);
						bouton_vert = 2;
						break;
					}
					break;
				case SDLK_RETURN:
					switch (bouton_vert)
					{
					case 1:
						but->y = y_vert;//disparition des boutons maintenant inutiles
						Fondu_disparition(boutons[3], screen);
						retourmenu = Menu_single(screen, destination1->y, set, sousmenu);//meu de selection du mode 1 joueur
						if (retourmenu == -1) {//le joueur decide de retourner dans la selection du nombre de joueurs
							but->y = y_vert;//réapparition des boutons maintenant utiles
							SDL_Rect logo;//creation du rectangle de destination du logo
							logo.x = (screen->w - boutons[4]->w) / 2;
							logo.y = 30;
							Fondu_disparition(boutons[3], screen);
							Fondu_apparition(NULL, screen, boutons[4], &logo);
							Fondu_apparition(but, screen, boutons[0], destination1);
							but->y = y_jaune;
							Fondu_apparition(but, screen, boutons[1], destination2);
							Fondu_apparition(but, screen, boutons[2], destination3);
							break;
						}
						else if (retourmenu == -2) {//quitter le jeu
						for(i=0; i<9; i++) SDL_FreeSurface(sousmenu[i]);
							return;
						}
						else {
							Fondu_disparition(boutons[3], screen);
							retourmenu = Choix_map(screen, map);
							if (retourmenu == -1) {
								but->y = y_vert;//réapparition des boutons maintenant utiles
								SDL_Rect logo;//creation du rectangle de destination du logo
								logo.x = (screen->w - boutons[4]->w) / 2;
								logo.y = 30;
								Fondu_disparition(boutons[3], screen);
								Fondu_apparition(NULL, screen, boutons[4], &logo);
								Fondu_apparition(but, screen, boutons[0], destination1);
								but->y = y_jaune;
								Fondu_apparition(but, screen, boutons[1], destination2);
								Fondu_apparition(but, screen, boutons[2], destination3);
								break;
							}
							else if (retourmenu == -2)
							{
								for(i=0; i<9; i++) SDL_FreeSurface(sousmenu[i]);
								return;
							}
							else
							{
								set->quit = FALSE;
								cont = 0;
								for(i=0; i<9; i++) SDL_FreeSurface(sousmenu[i]);
								break;
							}

						}
						break;
					case 2:
						but->y = y_vert;//disparition des boutons maintenant inutiles
						Fondu_disparition(boutons[3], screen);
						retourmenu = Menu_multiple(screen, destination1->y, set, sousmenu);//menu de selection du mode multijoueur
						if (retourmenu == -1) {
							but->y = y_vert;//réapparition des boutons maintenant utiles
							SDL_Rect logo;//creation du rectangle de destination du logo
							logo.x = (screen->w - boutons[4]->w) / 2;
							logo.y = 30;
							Fondu_disparition(boutons[3], screen);
							Fondu_apparition(NULL, screen, boutons[4], &logo);
							Fondu_apparition(but, screen, boutons[1], destination2);
							but->y = y_jaune;
							Fondu_apparition(but, screen, boutons[0], destination1);
							Fondu_apparition(but, screen, boutons[2], destination3);
							break;
						}
						else if (retourmenu == -2)
						{//quitter le jeu
						for(i=0; i<10; i++) SDL_FreeSurface(sousmenu[i]);
							return;
						}
						else {
							Fondu_disparition(boutons[3], screen);
							retourmenu = Choix_map(screen, map);
							if (retourmenu == -1) {
								but->y = y_vert;//réapparition des boutons maintenant utiles
								SDL_Rect logo;//creation du rectangle de destination du logo
								logo.x = (screen->w - boutons[4]->w) / 2;
								logo.y = 30;
								Fondu_disparition(boutons[3], screen);
								Fondu_apparition(NULL, screen, boutons[4], &logo);
								Fondu_apparition(but, screen, boutons[1], destination2);
								but->y = y_jaune;
								Fondu_apparition(but, screen, boutons[0], destination1);
								Fondu_apparition(but, screen, boutons[2], destination3);
								break;
							}
							else if (retourmenu == -2)
							{//quitter le jeu
							for(i=0; i<10; i++) SDL_FreeSurface(sousmenu[i]);
								return;
							}
							else
							{
								set->quit = FALSE;
								cont = 0;
								for(i=0; i<10; i++) SDL_FreeSurface(sousmenu[i]);
								break;
							}
						}
						break;
					case 3:
						set->quit = TRUE;
						cont = 0;
						return;
						break;
					}
				default:
					break;
					break;
				}
			default:
				break;
				break;
			}
		}
	}
	return;
}

int Menu_single(SDL_Surface *screen, int y, Choix *set, SDL_Surface **sousmenu) {
	int num_ia = 1, min = 1, heart = 1, separationx = 30, separationy = 42;
	bool time, life;
	//0=h_mode, 1=t_mode, 2=roll, 3=n_ia, 4=arrowup, 5=arrowdown, 6=ia, 7=retour, 8=go;
	SDL_Rect h, t, Roll, num, pos, initpos, retour, go;
	SDL_Rect position_nia, positionh, positiont, positionroll, posgo, posret;
	//chargement des images
	sousmenu[0] = SDL_LoadBMP("Image/h_mode.bmp");
	sousmenu[1] = SDL_LoadBMP("Image/t_mode.bmp");
	sousmenu[2] = SDL_LoadBMP("Image/roll.bmp");
	sousmenu[3] = SDL_LoadBMP("Image/player.bmp");
	sousmenu[4] = SDL_LoadBMP("Image/upArrow.bmp");
	sousmenu[5] = SDL_LoadBMP("Image/downArrow.bmp");
	sousmenu[6] = SDL_LoadBMP("Image/ia.bmp");
	sousmenu[7] = SDL_LoadBMP("Image/retour.bmp");
	sousmenu[8] = SDL_LoadBMP("Image/go.bmp");

	//parametrage
	initpos.x = (screen->w - sousmenu[6]->w - sousmenu[3]->w - sousmenu[5]->w - sousmenu[4]->w - separationx * 2) / 2;
	initpos.y = separationy;
	h.x = 0;
	h.y = 0;
	h.w = sousmenu[0]->w;
	h.h = sousmenu[0]->h / 3;
	t.x = 0;
	t.y = 0;
	t.w = sousmenu[1]->w;
	t.h = sousmenu[1]->h / 3;
	Roll.x = 0;
	Roll.y = 0;
	Roll.h = sousmenu[2]->h / 2;
	Roll.w = sousmenu[2]->w / 10;
	num.y = 0;
	num.h = sousmenu[3]->h / 2;
	num.w = sousmenu[3]->w / 5;
	num.x = num.w;
	retour.x = 0;
	retour.w = sousmenu[7]->w;
	retour.y = 0;
	retour.h = sousmenu[7]->h / 2;
	go.x = 0;
	go.w = sousmenu[8]->w;
	go.y = 0;
	go.h = sousmenu[8]->h / 2;
	//il n'y a qu'un seul joueur humain
	set->nb_joueur = 1;

	pos.x = initpos.x;
	pos.y = initpos.y;

	//definition des couleurs transparentes pour les fleches
	SDL_SetColorKey(sousmenu[5], SDL_SRCCOLORKEY, SDL_MapRGB(sousmenu[5]->format, 0, 0, 255));
	SDL_SetColorKey(sousmenu[4], SDL_SRCCOLORKEY, SDL_MapRGB(sousmenu[4]->format, 0, 0, 255));

	//positionnement de la ligne concernant les ia
	Fondu_apparition(NULL, screen, sousmenu[6], &pos);
	pos.x += sousmenu[6]->w + separationx;
	Fondu_apparition(&num, screen, sousmenu[3], &pos);
	position_nia.x = pos.x;
	position_nia.y = pos.y;
	pos.x += num.w + separationx;
	Fondu_apparition(NULL, screen, sousmenu[4], &pos);
	pos.x += sousmenu[4]->w;
	Fondu_apparition(NULL, screen, sousmenu[5], &pos);

	pos.y += separationy + num.h;

	//positionnement su choix du mode de jeu
	pos.x = (screen->w - sousmenu[0]->w - sousmenu[1]->w - 2 * separationx) / 2;
	positionh.x = pos.x;
	Fondu_apparition(&h, screen, sousmenu[0], &pos);
	pos.x += sousmenu[0]->w;
	positiont.x = pos.x;
	Fondu_apparition(&t, screen, sousmenu[1], &pos);
	positionh.y = pos.y;
	positiont.y = positionh.y;
	pos.y += separationy + num.h;

	//positonnemet de la "roue"
	pos.x = (screen->w - Roll.w - separationx) / 2;
	positionroll.x = pos.x;
	positionroll.y = pos.y;
	Fondu_apparition(&Roll, screen, sousmenu[2], &pos);
	pos.x += Roll.w + separationx;
	Fondu_apparition(NULL, screen, sousmenu[4], &pos);
	pos.x += sousmenu[4]->w;
	Fondu_apparition(NULL, screen, sousmenu[5], &pos);

	//positionement des boutons retour et go
	pos.x = separationx;
	pos.y = screen->h - retour.h - 24;
	posret.x = pos.x;
	posret.y = pos.y;
	Fondu_apparition(&retour, screen, sousmenu[7], &pos);
	pos.x = screen->w - separationx - go.w;
	pos.y = screen->h - 24 - go.h;
	posgo.x = pos.x;
	posgo.y = pos.y;
	Fondu_apparition(&go, screen, sousmenu[8], &posgo);


	//evenements
	SDL_Event event;
	int ligne_verte = 1, continuer = 1, droite4 = 1; //1: choix du nombre d'ia 2:choix du mode de jeu 3:choix des parametres du jeu 4:retour ou go
	num.y += num.h;
	h.y += 2 * h.h;
	SDL_BlitSurface(sousmenu[3], &num, screen, &position_nia);
	life = TRUE;
	SDL_BlitSurface(sousmenu[0], &h, screen, &positionh);
	SDL_Flip(screen);

	while (continuer)
	{
		while (SDL_WaitEvent(&event)) {
			switch (event.type)
			{
			case SDL_QUIT:
				set->quit = TRUE;
				continuer = 0;
				return -2;
				break;
			case SDL_KEYDOWN:
				switch (event.key.keysym.sym) {
				case SDLK_DOWN:
					if (ligne_verte == 1) {
						if (num_ia == 1) {
							break;
						}
						else {
							num.x -= num.w;
							num_ia--;
							SDL_BlitSurface(sousmenu[3], &num, screen, &position_nia);
							SDL_Flip(screen);
							set->nb_ia = num_ia;
							break;
						}
						break;
					}
					else if (ligne_verte == 2) {
						break;
					}
					else if (ligne_verte == 3) {
						if (life) {
							if (heart == 1) {
								break;
							}
							else {
								heart--;
								Roll.x -= Roll.w;
								SDL_BlitSurface(sousmenu[2], &Roll, screen, &positionroll);
								SDL_Flip(screen);
								set->vies = heart;
								break;
							}
						}
						if (time) {
							if (min == 1) {
								break;
							}
							else
							{
								min--;
								Roll.x -= Roll.w;
								SDL_BlitSurface(sousmenu[2], &Roll, screen, &positionroll);
								SDL_Flip(screen);
								set->duree = min;
								break;
							}
						}
					}
					else if (ligne_verte == 4)
					{
						break;
					}
				case SDLK_UP:
					if (ligne_verte == 1)
					{
						if (num_ia == 3) {
							break;
						}
						else
						{
							num_ia++;
							num.x += num.w;
							SDL_BlitSurface(sousmenu[3], &num, screen, &position_nia);
							SDL_Flip(screen);
							set->nb_ia = num_ia;
							break;
						}
					}
					else if (ligne_verte == 2)
					{
						break;
					}
					else if (ligne_verte == 3)
					{
						if (life)
						{
							if (heart == 5) {
								break;
							}
							else
							{
								heart++;
								Roll.x = (heart - 1)*Roll.w;
								SDL_BlitSurface(sousmenu[2], &Roll, screen, &positionroll);
								SDL_Flip(screen);
								set->vies = heart;
								break;
							}
						}
						if (time)
						{
							if (min == 5) {
								break;
							}
							else
							{
								min++;
								Roll.x = (min - 1)*Roll.w;
								SDL_BlitSurface(sousmenu[2], &Roll, screen, &positionroll);
								SDL_Flip(screen);
								set->duree = min;
								break;
							}
						}
					}
					else if (ligne_verte == 4)
					{
						break;
					}
				case SDLK_RIGHT:
					if (ligne_verte == 1)
					{
						break;
					}
					else if (ligne_verte == 2)
					{
						if (life) {
							life = FALSE;
							h.y -= h.h;
							time = TRUE;
							t.y += t.h;
							SDL_BlitSurface(sousmenu[0], &h, screen, &positionh);
							SDL_BlitSurface(sousmenu[1], &t, screen, &positiont);
							SDL_Flip(screen);
							set->mode = 1;
							break;
						}
						else
						{
							break;
						}
					}
					else if (ligne_verte == 3)
					{
						break;
					}
					else if (ligne_verte == 4)
					{
						if (droite4)
						{
							break;
						}
						else
						{
							retour.y = 0;
							go.y = sousmenu[8]->h / 2;
							SDL_BlitSurface(sousmenu[7], &retour, screen, &posret);
							SDL_BlitSurface(sousmenu[8], &go, screen, &posgo);
							SDL_Flip(screen);
							droite4 = 1;
							break;
						}
					}
				case SDLK_LEFT:
					if (ligne_verte == 1)
					{
						break;
					}
					else if (ligne_verte == 2)
					{
						if (life)
						{
							break;
						}
						else
						{
							life = TRUE;
							time = FALSE;
							h.y = sousmenu[0]->h / 3;//passage au vert
							t.y = 0;//retour au jaune
							SDL_BlitSurface(sousmenu[0], &h, screen, &positionh);
							SDL_BlitSurface(sousmenu[1], &t, screen, &positiont);
							SDL_Flip(screen);
							set->mode = 0;
							break;
						}
					}
					else if (ligne_verte == 3)
					{
						break;
					}
					else if (ligne_verte == 4)
					{
						if (droite4) {
							retour.y = sousmenu[7]->h / 2;
							go.y = 0;
							SDL_BlitSurface(sousmenu[7], &retour, screen, &posret);
							SDL_BlitSurface(sousmenu[8], &go, screen, &posgo);
							SDL_Flip(screen);
							droite4 = 0;
							break;
						}
						else
						{
							break;
						}
					}
				case SDLK_RETURN:
					if (ligne_verte == 1)
					{
						ligne_verte++;
						set->nb_ia = num_ia;
						num.y = 0;//retour au jaune de la ligne precedente
						SDL_BlitSurface(sousmenu[3], &num, screen, &position_nia);
						if (life) {
							h.y = sousmenu[0]->h / 3;//passage au vert
							SDL_BlitSurface(sousmenu[0], &h, screen, &positionh);
							SDL_Flip(screen);
							break;
						}
						else
						{
							t.y = sousmenu[1]->h / 3;//passage au vert
							SDL_BlitSurface(sousmenu[1], &t, screen, &positiont);
							SDL_Flip(screen);
							break;
						}

					}
					else if (ligne_verte == 2) {
						ligne_verte++;
						if (life)
						{//h est vert et t est jaune
							set->mode = 0;
							h.y = sousmenu[0]->h * 2 / 3;//passage au bleu
							SDL_BlitSurface(sousmenu[0], &h, screen, &positionh);
							Roll.x = (heart - 1)*Roll.w;
							Roll.y = sousmenu[2]->h / 2;
							SDL_BlitSurface(sousmenu[2], &Roll, screen, &positionroll);
							SDL_Flip(screen);
							break;
						}
						else
						{//t est vert et h est jaune
							set->mode = 1;
							t.y = sousmenu[1]->h * 2 / 3;//passage au bleu
							SDL_BlitSurface(sousmenu[1], &t, screen, &positiont);
							Roll.y = sousmenu[2]->h / 2;
							Roll.x = (min - 1)*Roll.w;
							SDL_BlitSurface(sousmenu[2], &Roll, screen, &positionroll);
							SDL_Flip(screen);
							break;
						}
					}
					else if (ligne_verte == 3)
					{
						ligne_verte++;
						if (life) set->vies = heart;
						else set->duree = min;
						if (droite4) {
							go.y = sousmenu[8]->h / 2;
							retour.y = 0;
							Roll.y = 0;
							SDL_BlitSurface(sousmenu[7], &retour, screen, &posret);
							SDL_BlitSurface(sousmenu[2], &Roll, screen, &positionroll);
							SDL_BlitSurface(sousmenu[8], &go, screen, &posgo);
							SDL_Flip(screen);
							break;
						}
						else
						{
							go.y = 0;
							retour.y = sousmenu[7]->h / 2;
							Roll.y = 0;
							SDL_BlitSurface(sousmenu[7], &retour, screen, &posret);
							SDL_BlitSurface(sousmenu[2], &Roll, screen, &positionroll);
							SDL_BlitSurface(sousmenu[8], &go, screen, &posgo);
							SDL_Flip(screen);
							break;
						}
					}
					else if (ligne_verte == 4)
					{
						if (droite4) {
							set->quit = FALSE;
							return 1;
						}
						else
						{
							return -1;
						}
					}
				case SDLK_BACKSPACE:
					if (ligne_verte == 1)
					{
						break;
					}
					else if (ligne_verte == 2)
					{
						ligne_verte--;
						num.y = sousmenu[3]->h / 2;
						SDL_BlitSurface(sousmenu[3], &num, screen, &position_nia);
						if (life) {//h est vert t est jaune
							h.y = sousmenu[0]->h * 2 / 3;//passage au bleu
							SDL_BlitSurface(sousmenu[0], &h, screen, &positionh);
							SDL_Flip(screen);
							break;
						}
						else
						{//t est vert et h est jaune
							t.y = sousmenu[1]->h * 2 / 3;//passage au bleu
							SDL_BlitSurface(sousmenu[1], &t, screen, &positiont);
							SDL_Flip(screen);
							break;
						}

					}
					else if (ligne_verte == 3)
					{
						ligne_verte--;
						Roll.y = 0;
						SDL_BlitSurface(sousmenu[2], &Roll, screen, &positionroll);
						if (life)
						{
							h.y = sousmenu[0]->h / 3;//passage au vert
							SDL_BlitSurface(sousmenu[0], &h, screen, &positionh);
							SDL_Flip(screen);
							break;
						}
						else
						{//passage au vert
							t.y = sousmenu[1]->h / 3;
							SDL_BlitSurface(sousmenu[1], &t, screen, &positiont);
							SDL_Flip(screen);
							break;
						}
					}
					else if (ligne_verte == 4) {
						ligne_verte--;
						Roll.y = sousmenu[2]->h / 2;
						SDL_BlitSurface(sousmenu[2], &Roll, screen, &positionroll);
						if (droite4) {
							go.y = 0;
							SDL_BlitSurface(sousmenu[8], &go, screen, &posgo);
							SDL_Flip(screen);
							break;
						}
						else
						{
							retour.y = 0;
							SDL_BlitSurface(sousmenu[7], &retour, screen, &posret);
							SDL_Flip(screen);
							break;
						}
					}
				default:
					break;
				}
				break;
			default:
				break;
			}
		}
	}
	return 0;
}

int Menu_multiple(SDL_Surface *screen, int y, Choix *set, SDL_Surface **sousmenu) {
	int num_ia = 0, num_j = 1, min = 1, heart = 1, separationx = 30, separationy = 42;
	bool time, life;
	//0=h_mode, 1=t_mode, 2=roll, 3=n_ia, 4=player, 5=arrowup, 6=arrowdown, 7=ia, 8=retour, 9=go;
	SDL_Rect h, t, Roll, num, pos, initpos, retour, go;
	SDL_Rect position_num, position_nia, positionh, positiont, positionroll, posgo, posret;
	sousmenu[0] = SDL_LoadBMP("Image/h_mode.bmp");
	sousmenu[1] = SDL_LoadBMP("Image/t_mode.bmp");
	sousmenu[2] = SDL_LoadBMP("Image/roll.bmp");
	sousmenu[3] = SDL_LoadBMP("Image/player.bmp");
	sousmenu[4] = SDL_LoadBMP("Image/p_but.bmp");
	sousmenu[5] = SDL_LoadBMP("Image/upArrow.bmp");
	sousmenu[6] = SDL_LoadBMP("Image/downArrow.bmp");
	sousmenu[7] = SDL_LoadBMP("Image/ia.bmp");
	sousmenu[8] = SDL_LoadBMP("Image/retour.bmp");
	sousmenu[9] = SDL_LoadBMP("Image/go.bmp");

	//parametrages
	initpos.x = (screen->w - sousmenu[6]->w - sousmenu[3]->w - sousmenu[5]->w - sousmenu[4]->w - separationx * 2) / 2;
	initpos.y = separationy;
	h.x = 0;
	h.y = 0;
	h.w = sousmenu[0]->w;
	h.h = sousmenu[0]->h / 3;
	t.x = 0;
	t.y = 0;
	t.w = sousmenu[1]->w;
	t.h = sousmenu[1]->h / 3;
	Roll.x = 0;
	Roll.y = 0;
	Roll.h = sousmenu[2]->h / 2;
	Roll.w = sousmenu[2]->w / 10;
	num.x = 0;
	num.y = 0;
	num.h = sousmenu[3]->h / 2;
	num.w = sousmenu[3]->w / 5;
	retour.x = 0;
	retour.w = sousmenu[8]->w;
	retour.y = 0;
	retour.h = sousmenu[8]->h / 2;
	go.x = 0;
	go.w = sousmenu[9]->w;
	go.y = 0;
	go.h = sousmenu[9]->h / 2;

	pos.x = initpos.x;
	pos.y = initpos.y;

	//definition des couleurs transparentes pour les fleches
	SDL_SetColorKey(sousmenu[5], SDL_SRCCOLORKEY, SDL_MapRGB(sousmenu[5]->format, 0, 0, 255));
	SDL_SetColorKey(sousmenu[6], SDL_SRCCOLORKEY, SDL_MapRGB(sousmenu[6]->format, 0, 0, 255));

	//positionnement de la ligne concernant les ia
	Fondu_apparition(NULL, screen, sousmenu[7], &pos);
	pos.x += sousmenu[7]->w + separationx;
	Fondu_apparition(&num, screen, sousmenu[3], &pos);
	position_nia.x = pos.x;
	position_nia.y = pos.y;
	pos.x += num.w + separationx;
	Fondu_apparition(NULL, screen, sousmenu[5], &pos);
	pos.x += sousmenu[5]->w;
	Fondu_apparition(NULL, screen, sousmenu[6], &pos);

	//positionement de la ligne concernant les joueurs
	pos.x = initpos.x;
	pos.y += separationy + num.h;

	Fondu_apparition(NULL, screen, sousmenu[4], &pos);
	pos.x += sousmenu[4]->w + separationx;
	num.x = num.w;
	Fondu_apparition(&num, screen, sousmenu[3], &pos);
	position_num.x = pos.x;
	position_num.y = pos.y;
	pos.x += num.w + separationx;
	Fondu_apparition(NULL, screen, sousmenu[5], &pos);
	pos.x += sousmenu[5]->w;
	Fondu_apparition(NULL, screen, sousmenu[6], &pos);

	pos.y += separationy + num.h;

	//positionnement su choix du mode de jeu
	pos.x = (screen->w - sousmenu[0]->w - sousmenu[1]->w - 2 * separationx) / 2;
	positionh.x = pos.x;
	Fondu_apparition(&h, screen, sousmenu[0], &pos);
	pos.x += sousmenu[0]->w;
	positiont.x = pos.x;
	Fondu_apparition(&t, screen, sousmenu[1], &pos);
	positionh.y = pos.y;
	positiont.y = positionh.y;
	pos.y += separationy + num.h;

	//positonnemet de la "roue"
	pos.x = (screen->w - Roll.w - separationx) / 2;
	positionroll.x = pos.x;
	positionroll.y = pos.y;
	Fondu_apparition(&Roll, screen, sousmenu[2], &pos);
	pos.x += Roll.w + separationx;
	Fondu_apparition(NULL, screen, sousmenu[5], &pos);
	pos.x += sousmenu[5]->w;
	Fondu_apparition(NULL, screen, sousmenu[6], &pos);

	//positionement des boutons retour et go
	pos.x = separationx;
	pos.y = screen->h - retour.h - 24;
	posret.x = pos.x;
	posret.y = pos.y;
	Fondu_apparition(&retour, screen, sousmenu[8], &pos);
	pos.x = screen->w - separationx - go.w;
	pos.y = screen->h - 24 - go.h;
	posgo.x = pos.x;
	posgo.y = pos.y;
	Fondu_apparition(&go, screen, sousmenu[9], &posgo);

	//evenements
	SDL_Event event;
	int ligne_verte = 1, continuer = 1, droite4 = 1; //1: choix du nombre d'ia 2:choix du mode de jeu 3:choix des parametres du jeu 4:retour ou go
	num.x = num_ia*num.w;
	num.y += num.h;
	h.y += 2 * h.h;
	SDL_BlitSurface(sousmenu[3], &num, screen, &position_nia);
	life = TRUE;
	SDL_BlitSurface(sousmenu[0], &h, screen, &positionh);
	SDL_Flip(screen);

	while (continuer)
	{
		while (SDL_WaitEvent(&event)) {
			switch (event.type)
			{
			case SDL_QUIT:
				set->quit = TRUE;
				return -2;
				break;
			case SDL_KEYDOWN:
				switch (event.key.keysym.sym)
				{
				case SDLK_DOWN:
					if (ligne_verte == 1) {
						if (num_ia == 0) {
							break;
						}
						else {
							num_ia--;
							num.x = num_ia*num.w;
							SDL_BlitSurface(sousmenu[3], &num, screen, &position_nia);
							SDL_Flip(screen);
							set->nb_ia = num_ia;
							break;
						}
						break;
					}
					else if (ligne_verte == 2) {
						if (num_j == 1) {
							break;
						}
						else {
							num.x -= num.w;
							num_j--;
							SDL_BlitSurface(sousmenu[3], &num, screen, &position_num);
							SDL_Flip(screen);
							set->nb_joueur = num_j;
							break;
						}
						break;
					}
					else if (ligne_verte == 3) {
						break;
					}
					else if (ligne_verte == 4)
					{
						if (life) {
							if (heart == 1) {
								break;
							}
							else {
								heart--;
								Roll.x -= Roll.w;
								SDL_BlitSurface(sousmenu[2], &Roll, screen, &positionroll);
								SDL_Flip(screen);
								set->vies = heart;
								break;
							}
						}
						if (time) {
							if (min == 1) {
								break;
							}
							else
							{
								min--;
								Roll.x -= Roll.w;
								SDL_BlitSurface(sousmenu[2], &Roll, screen, &positionroll);
								SDL_Flip(screen);
								set->duree = min;
								break;
							}
						}
					}
					else if (ligne_verte == 5) {
						break;
					}
				case SDLK_UP:
					if (ligne_verte == 1) {
						if (num_ia + num_j == 4) {
							break;
						}
						else
						{
							num_ia++;
							num.x += num.w;
							SDL_BlitSurface(sousmenu[3], &num, screen, &position_nia);
							SDL_Flip(screen);
							set->nb_ia = num_ia;
							break;
						}
					}
					else if (ligne_verte == 2)
					{
						if (num_ia + num_j == 4) {
							break;
						}
						else
						{
							if (num_j < 2) {
								num_j++;
								num.x += num.w;
								SDL_BlitSurface(sousmenu[3], &num, screen, &position_num);
								SDL_Flip(screen);
								set->nb_joueur = num_j;
							}

							break;
						}
					}
					else if (ligne_verte == 3) {
						break;
					}
					else if (ligne_verte == 4)
					{
						if (life)
						{
							if (heart == 5) {
								break;
							}
							else
							{
								heart++;
								Roll.x = (heart - 1) * Roll.w;
								SDL_BlitSurface(sousmenu[2], &Roll, screen, &positionroll);
								SDL_Flip(screen);
								set->vies = heart;
								break;
							}
						}
						if (time)
						{
							if (min == 5) {
								break;
							}
							else
							{
								min++;
								Roll.x = (min - 1) * Roll.w;
								SDL_BlitSurface(sousmenu[2], &Roll, screen, &positionroll);
								SDL_Flip(screen);
								set->duree = min;
								break;
							}
						}
					}
					else if (ligne_verte == 5)
					{
						break;
					}
				case SDLK_RIGHT:
					if (ligne_verte == 1) {
						break;
					}
					else if (ligne_verte == 2) {
						break;
					}
					else if (ligne_verte == 3) {
						if (life) {
							life = FALSE;
							h.y -= h.h;
							time = TRUE;
							t.y += t.h;
							SDL_BlitSurface(sousmenu[0], &h, screen, &positionh);
							SDL_BlitSurface(sousmenu[1], &t, screen, &positiont);
							SDL_Flip(screen);
							set->mode = 1;
							break;
						}
						else
						{
							break;
						}
					}
					else if (ligne_verte == 4) {
						break;
					}
					else if (ligne_verte == 5) {
						if (droite4)
						{
							break;
						}
						else
						{
							retour.y = 0;
							go.y = sousmenu[9]->h / 2;
							SDL_BlitSurface(sousmenu[8], &retour, screen, &posret);
							SDL_BlitSurface(sousmenu[9], &go, screen, &posgo);
							SDL_Flip(screen);
							droite4 = 1;
							break;
						}
					}
				case SDLK_LEFT:
					if (ligne_verte == 1) {
						break;
					}
					else if (ligne_verte == 2) {
						break;
					}
					else if (ligne_verte == 3) {
						if (life)
						{
							break;
						}
						else
						{
							life = TRUE;
							time = FALSE;
							h.y = sousmenu[0]->h / 3;//passage au vert
							t.y = 0;//retour au jaune
							SDL_BlitSurface(sousmenu[0], &h, screen, &positionh);
							SDL_BlitSurface(sousmenu[1], &t, screen, &positiont);
							SDL_Flip(screen);
							set->mode = 0;
							break;
						}
					}
					else if (ligne_verte == 4) {
						break;
					}
					else if (ligne_verte == 5) {
						if (droite4) {
							retour.y = sousmenu[8]->h / 2;
							go.y = 0;
							SDL_BlitSurface(sousmenu[8], &retour, screen, &posret);
							SDL_BlitSurface(sousmenu[9], &go, screen, &posgo);
							SDL_Flip(screen);
							droite4 = 0;
							break;
						}
						else
						{
							break;
						}
					}
				case SDLK_RETURN:
					if (ligne_verte == 1)
					{
						ligne_verte++;
						set->nb_ia = num_ia;
						num.y = 0;//retour au jaune de la ligne precedente
						SDL_BlitSurface(sousmenu[3], &num, screen, &position_nia);
						num.y = sousmenu[3]->h / 2;
						num.x = num_j*num.w;
						SDL_BlitSurface(sousmenu[3], &num, screen, &position_num);
						SDL_Flip(screen);
						break;
					}
					else if (ligne_verte == 2) {
						ligne_verte++;
						set->nb_joueur = num_j;
						num.y = 0;//retour au jaune de la ligne precedente
						SDL_BlitSurface(sousmenu[3], &num, screen, &position_num);
						if (life) {
							h.y = sousmenu[0]->h / 3;//passage au vert
							SDL_BlitSurface(sousmenu[0], &h, screen, &positionh);
							SDL_Flip(screen);
							break;
						}
						else
						{
							t.y = sousmenu[1]->h / 3;//passage au vert
							SDL_BlitSurface(sousmenu[1], &t, screen, &positiont);
							SDL_Flip(screen);
							break;
						}
					}
					else if (ligne_verte == 3) {
						ligne_verte++;
						if (life)
						{//h est vert et t est jaune
							set->mode = 0;
							h.y = sousmenu[0]->h * 2 / 3;//passage au bleu
							SDL_BlitSurface(sousmenu[0], &h, screen, &positionh);
							Roll.y = sousmenu[2]->h / 2;
							Roll.x = (heart - 1)*Roll.w;
							SDL_BlitSurface(sousmenu[2], &Roll, screen, &positionroll);
							SDL_Flip(screen);
							break;
						}
						else
						{//t est vert et h est jaune
							set->mode = 1;
							t.y = sousmenu[1]->h * 2 / 3;//passage au bleu
							SDL_BlitSurface(sousmenu[1], &t, screen, &positiont);
							Roll.y = sousmenu[2]->h / 2;
							Roll.x = (min - 1)*Roll.w;
							SDL_BlitSurface(sousmenu[2], &Roll, screen, &positionroll);
							SDL_Flip(screen);
							break;
						}
					}
					else if (ligne_verte == 4) {
						ligne_verte++;
						if (life) set->vies = heart;
						else set->duree = min;
						if (droite4) {
							go.y = sousmenu[9]->h / 2;
							retour.y = 0;
							Roll.y = 0;
							SDL_BlitSurface(sousmenu[8], &retour, screen, &posret);
							SDL_BlitSurface(sousmenu[2], &Roll, screen, &positionroll);
							SDL_BlitSurface(sousmenu[9], &go, screen, &posgo);
							SDL_Flip(screen);
							break;
						}
						else
						{
							go.y = 0;
							retour.y = sousmenu[8]->h / 2;
							Roll.y = 0;
							SDL_BlitSurface(sousmenu[8], &retour, screen, &posret);
							SDL_BlitSurface(sousmenu[2], &Roll, screen, &positionroll);
							SDL_BlitSurface(sousmenu[9], &go, screen, &posgo);
							SDL_Flip(screen);
							break;
						}
					}
					else if (ligne_verte == 5) {
						if (droite4) {
							set->quit = FALSE;
							return 1;
						}
						else
						{
							return -1;
						}
					}
				case SDLK_BACKSPACE:
					if (ligne_verte == 1) {
						break;
					}
					else if (ligne_verte == 2) {
						ligne_verte--;
						num.y = 0;
						SDL_BlitSurface(sousmenu[3], &num, screen, &position_num);//ligne 2 jaune
						num.y = sousmenu[3]->h / 2;
						num.x = num_ia*num.w;
						SDL_BlitSurface(sousmenu[3], &num, screen, &position_nia);//ligne 1 verte
						SDL_Flip(screen);
						break;
					}
					else if (ligne_verte == 3) {
						ligne_verte--;
						num.y = sousmenu[3]->h / 2;
						SDL_BlitSurface(sousmenu[3], &num, screen, &position_num);
						if (life) {//h est vert t est jaune
							h.y = sousmenu[0]->h * 2 / 3;//passage au bleu
							SDL_BlitSurface(sousmenu[0], &h, screen, &positionh);
							SDL_Flip(screen);
							break;
						}
						else
						{//t est vert et h est jaune
							t.y = sousmenu[1]->h * 2 / 3;//passage au bleu
							SDL_BlitSurface(sousmenu[1], &t, screen, &positiont);
							SDL_Flip(screen);
							break;
						}
					}
					else if (ligne_verte == 4) {
						ligne_verte--;
						Roll.y = 0;
						SDL_BlitSurface(sousmenu[2], &Roll, screen, &positionroll);
						if (life)
						{
							h.y = sousmenu[0]->h / 3;//passage au vert
							SDL_BlitSurface(sousmenu[0], &h, screen, &positionh);
							SDL_Flip(screen);
							break;
						}
						else
						{//passage au vert
							t.y = sousmenu[1]->h / 3;
							SDL_BlitSurface(sousmenu[1], &t, screen, &positiont);
							SDL_Flip(screen);
							break;
						}
					}
					else if (ligne_verte == 5) {
						ligne_verte--;
						Roll.y = sousmenu[2]->h / 2;
						SDL_BlitSurface(sousmenu[2], &Roll, screen, &positionroll);
						if (droite4) {
							go.y = 0;
							SDL_BlitSurface(sousmenu[9], &go, screen, &posgo);
							SDL_Flip(screen);
							break;
						}
						else
						{
							retour.y = 0;
							SDL_BlitSurface(sousmenu[8], &retour, screen, &posret);
							SDL_Flip(screen);
							break;
						}
					}
					break;
				default:
					break;
				}
				break;
			default:
				break;
			}
		}
	}
	return 0;
}

int Choix_map(SDL_Surface *screen, int *map) {
	int separationx = 30, /*separationy = 42,*/ ligne_courante = 1, droite = 1, currentmap = 1;
	SDL_Surface *Map, *Num, *arrowup, *arrowdown, *go, *retour;
	SDL_Rect num, pos, butgo, butret;
	SDL_Rect posnum, posgo, posret;
	SDL_Event event;

	//parametrage
	Map = SDL_LoadBMP("Image/map.bmp");
	Num = SDL_LoadBMP("Image/roll.bmp");
	arrowup = SDL_LoadBMP("Image/upArrow.bmp");
	arrowdown = SDL_LoadBMP("Image/downArrow.bmp");
	go = SDL_LoadBMP("Image/go.bmp");
	retour = SDL_LoadBMP("Image/retour.bmp");

	butgo.x = 0;
	butgo.y = 0;
	butgo.h = go->h / 2;
	butgo.w = go->w;
	butret.x = 0;
	butret.y = 0;
	butret.h = retour->h / 2;
	butret.w = retour->w;
	num.x = 0;
	num.y = 0;
	num.h = Num->h / 2;
	num.w = Num->w / 10;
	pos.y = (screen->h - Num->h) / 2;
	pos.x = (screen->w - Num->w - 2 * arrowdown->w) / 2;

	SDL_SetColorKey(arrowdown, SDL_SRCCOLORKEY, SDL_MapRGB(arrowdown->format, 0, 0, 255));
	SDL_SetColorKey(arrowup, SDL_SRCCOLORKEY, SDL_MapRGB(arrowup->format, 0, 0, 255));

	Fondu_apparition(NULL, screen, Map, &pos);
	pos.x += Map->w;
	posnum = pos;
	Fondu_apparition(&num, screen, Num, &pos);
	pos.x += num.w;
	Fondu_apparition(NULL, screen, arrowup, &pos);
	pos.x += arrowdown->w;
	Fondu_apparition(NULL, screen, arrowdown, &pos);

	posret.x = separationx;
	posret.y = screen->h - butret.h - 24;
	posgo.x = screen->w - separationx - butgo.w;
	posgo.y = screen->h - 24 - butgo.h;

	Fondu_apparition(&butgo, screen, go, &posgo);
	Fondu_apparition(&butret, screen, retour, &posret);

	int cont = 1;
	map[0] = currentmap;
	num.y = num.h;
	SDL_BlitSurface(Num, &num, screen, &posnum);
	SDL_Flip(screen);
	while (cont)
	{
		while (SDL_WaitEvent(&event)) {
			switch (event.type)
			{
			case SDL_QUIT:
                SDL_FreeSurface(retour);
                SDL_FreeSurface(Num);
                SDL_FreeSurface(go);
                SDL_FreeSurface(Map);
                SDL_FreeSurface(arrowdown);
                SDL_FreeSurface(arrowup);
				return -2;
				break;
			case SDL_KEYDOWN:
				switch (event.key.keysym.sym)
				{
				case SDLK_UP:
					if (ligne_courante == 1)
					{
						if (currentmap == NOMBRE_CARTES)
						{
							break;
						}
						else
						{
							num.x += num.w;
							SDL_BlitSurface(Num, &num, screen, &posnum);
							SDL_Flip(screen);
							currentmap++;
							map[0] = currentmap;
							break;
						}
					}
					else {
						break;
					}
				case SDLK_DOWN:
					if (ligne_courante == 1) {
						if (currentmap == 1) {
							break;
						}
						else
						{
							num.x -= num.w;
							SDL_BlitSurface(Num, &num, screen, &posnum);
							SDL_Flip(screen);
							currentmap--;
							map[0] = currentmap;
							break;
						}
					}
					else
					{
						break;
					}
				case SDLK_RIGHT:
					if (ligne_courante == 1) {
						break;
					}
					else
					{
						if (droite)
						{
							break;
						}
						else
						{
							butret.y = 0;
							butgo.y = go->h / 2;
							SDL_BlitSurface(retour, &butret, screen, &posret);
							SDL_BlitSurface(go, &butgo, screen, &posgo);
							SDL_Flip(screen);
							droite = 1;
							break;
						}
					}
				case SDLK_LEFT:
					if (ligne_courante == 1)
					{
						break;
					}
					else
					{
						if (droite) {
							butret.y = retour->h / 2;
							butgo.y = 0;
							SDL_BlitSurface(retour, &butret, screen, &posret);
							SDL_BlitSurface(go, &butgo, screen, &posgo);
							SDL_Flip(screen);
							droite = 0;
							break;
						}
						else
						{
							break;
						}
					}
				case SDLK_RETURN:
					if (ligne_courante == 1)
					{
						ligne_courante++;
						if (droite)
						{
							butgo.y = go->h / 2;
							butret.y = 0;
							num.y = 0;
							SDL_BlitSurface(retour, &butret, screen, &posret);
							SDL_BlitSurface(Num, &num, screen, &posnum);
							SDL_BlitSurface(go, &butgo, screen, &posgo);
							SDL_Flip(screen);
							break;
						}
						else
						{
							butgo.y = 0;
							butret.y = retour->h / 2;
							num.y = 0;
							SDL_BlitSurface(retour, &butret, screen, &posret);
							SDL_BlitSurface(Num, &num, screen, &posnum);
							SDL_BlitSurface(go, &butgo, screen, &posgo);
							SDL_Flip(screen);
							break;
						}
					}
					else
					{
						if (droite) {
                            SDL_FreeSurface(retour);
                            SDL_FreeSurface(Num);
                            SDL_FreeSurface(go);
                            SDL_FreeSurface(Map);
                            SDL_FreeSurface(arrowdown);
                            SDL_FreeSurface(arrowup);
							return 1;
							break;
						}
						else
						{
							cont = 0;
                            SDL_FreeSurface(retour);
                            SDL_FreeSurface(Num);
                            SDL_FreeSurface(go);
                            SDL_FreeSurface(Map);
                            SDL_FreeSurface(arrowdown);
                            SDL_FreeSurface(arrowup);
							return -1;
						}
					}
				case SDLK_BACKSPACE:
					if (ligne_courante == 1)
					{
						break;
					}
					else {
						ligne_courante--;
						num.y = Num->h / 2;
						SDL_BlitSurface(Num, &num, screen, &posnum);
						if (droite) {
							butgo.y = 0;
							SDL_BlitSurface(go, &butgo, screen, &posgo);
							SDL_Flip(screen);
							break;
						}
						else
						{
							butret.y = 0;
							SDL_BlitSurface(retour, &butret, screen, &posret);
							SDL_Flip(screen);
							break;
						}
					}
				default:
					break;
				}
				break;
			default:
				break;
			}
		}
	}
	return 0;
}

void Scores(jeu *game, SDL_Surface *screen, int *winner, int *comp, int nwin) {
	SDL_Surface *roll, *oui_non, *againb, *fond, *win;
	Mix_Music *musique;
	SDL_Rect pos, bigroll, rep;
	int i, temp, oui = 1, cont = 1;

	roll = SDL_LoadBMP("Image/megaroll.bmp");
	oui_non = SDL_LoadBMP("Image/oui_non.bmp");
	fond = SDL_LoadBMP("Image/fond.bmp");
	againb = SDL_LoadBMP("Image/play again.bmp");
	win = SDL_LoadBMP("Image/winbut.bmp");
    musique = Mix_LoadMUS("Son/The Calm.wav");

    Mix_VolumeMusic(MIX_MAX_VOLUME/5);
	Mix_PlayMusic(musique, -1);

	Fondu_disparition(fond, screen);
	rep.x = 0;
	rep.y = 0;
	rep.w = oui_non->w;
	rep.h = oui_non->h / 2;

	bigroll.w = roll->w / 10;
	bigroll.h = roll->h / 4;

	pos.x = 15;
	pos.y = 15;

	Fondu_apparition(NULL, screen, win, &pos);
	pos.x += win->w + 10;
	temp = pos.x;
	bigroll.y = 0;
	bigroll.x = (winner[0] - 1) * bigroll.w;
	Fondu_apparition(&bigroll, screen, roll, &pos);
	pos.x += bigroll.w;
	if (comp[winner[0] - 1] <= 0) {
		bigroll.x = 0;
		bigroll.y = bigroll.h * 3;
	}
	else if (comp[winner[0] - 1] < 11 && comp[winner[0] - 1]>0) {
		bigroll.x = (comp[winner[0] - 1]-1)*bigroll.w;
		bigroll.y = 0;
	}
	else if (comp[winner[0] - 1] >= 11 && comp[winner[0] - 1]<21) {
		bigroll.y = bigroll.h;
		bigroll.x = (comp[winner[0] - 1] - 11) * bigroll.w;
	}
	else if (comp[winner[0] - 1] >= 21) {
		bigroll.y = 2 * bigroll.h;
		bigroll.x = (comp[winner[0] - 1] - 21) * bigroll.w;
	}
	Fondu_apparition(&bigroll, screen, roll, &pos);
	if (nwin > 1) {
		for (i = 1; i < nwin; i++) {
			pos.x = temp;
			bigroll.x = (winner[i] - 1) * bigroll.w;
			bigroll.y = 0;
			pos.y += bigroll.h;
			Fondu_apparition(&bigroll, screen, roll, &pos);
			if (comp[i] <= 0) {
				bigroll.x = 0;
				bigroll.y = bigroll.h * 3;
			}
			else if (comp[i] < 11 && comp[i]>0) {
				bigroll.x = (comp[i]-1)*bigroll.w;
				bigroll.y = 0;
			}
			else if (comp[i] >= 11&& comp[i]<21) {
				bigroll.y = bigroll.h;
				bigroll.x = (comp[i] - 11)*bigroll.w;
			}
			else if (comp[i] >= 21) {
				bigroll.y = 2 * bigroll.h;
				bigroll.x = (comp[i] - 21)*bigroll.w;
			}
			pos.x += bigroll.w;
			Fondu_apparition(&bigroll, screen, roll, &pos);
		}
	}
	pos.y = screen->h - bigroll.h - 15;
	pos.x = 15;
	Fondu_apparition(NULL, screen, againb, &pos);

	pos.x += againb->w + 5;
	Fondu_apparition(&rep, screen, oui_non, &pos);

	SDL_Event event;

	while (cont) {
		SDL_PollEvent(&event);
		switch (event.type)
		{
		case SDL_QUIT:
			game->again = FALSE;
			cont = 0;
			break;
		case SDL_KEYDOWN:
			switch (event.key.keysym.sym)
			{
			case SDLK_RIGHT:
				if (oui) {
					oui = 0;
					rep.y = rep.h;
					SDL_BlitSurface(oui_non, &rep, screen, &pos);
					SDL_Flip(screen);
				}
				break;
			case SDLK_LEFT:
				if (!oui) {
					oui = 1;
					rep.y = 0;
					SDL_BlitSurface(oui_non, &rep, screen, &pos);
					SDL_Flip(screen);
				}
				break;
			case SDLK_RETURN:
				if (oui) {
					game->again = TRUE;
					cont = 0;
				}
				else
				{
					game->again = FALSE;
					cont = 0;
				}
				break;
                default:
                    break;
			}

			break;
		default:
			break;
		}
	}
	SDL_FreeSurface(roll);
	SDL_FreeSurface(win);
	SDL_FreeSurface(againb);
	SDL_FreeSurface(oui_non);
	SDL_FreeSurface(fond);
	Mix_PauseMusic();
    Mix_FreeMusic(musique);

	return;
}
